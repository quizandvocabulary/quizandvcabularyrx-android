package me.asddsajpgnoob.quizandvocabularyrx.ui.interfaceSettings

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.InterfaceSettingsRepository

class InterfaceSettingsViewModel(
    private val interfaceSettingsRepository: InterfaceSettingsRepository
) : ViewModel() {

    val darkMode = interfaceSettingsRepository.darkMode

    val defaultTab = interfaceSettingsRepository.defaultTab

    val showAudiosInVocabularyScreen = interfaceSettingsRepository.showAudiosInVocabularyScreen

    val showExamplesInVocabularyScreen = interfaceSettingsRepository.showExamplesInVocabularyScreen

    val openTranslatorOnTopOfApp = interfaceSettingsRepository.openTranslatorOnTopOfApp

    val saveNavigationStateWhenSwitchingTab =
        interfaceSettingsRepository.saveNavigationStateWhenSwitchingTab

    fun onRadioButtonDarkClicked() {
        interfaceSettingsRepository.setDarkMode(DarkModePreferences.DARK)
    }

    fun onRadioButtonLightClicked() {
        interfaceSettingsRepository.setDarkMode(DarkModePreferences.LIGHT)
    }

    fun onRadioButtonDeviceThemeClicked() {
        interfaceSettingsRepository.setDarkMode(DarkModePreferences.FOLLOW_SYSTEM)
    }

    fun onRadioButtonVocabularyClicked() {
        interfaceSettingsRepository.setDefaultTab(DefaultTabPreferences.VOCABULARY)
    }

    fun onRadioButtonQuizClicked() {
        interfaceSettingsRepository.setDefaultTab(DefaultTabPreferences.QUIZ)
    }

    fun onCheckBoxShowAudiosClicked(isChecked: Boolean) {
        interfaceSettingsRepository.setShowAudiosInVocabularyScreen(isChecked)
    }

    fun onCheckboxShowExamplesClicked(isChecked: Boolean) {
        interfaceSettingsRepository.setExamplesAudiosInVocabularyScreen(isChecked)
    }

    fun onCheckBoxOpenTranslatorOnTopOfAppClicked(isChecked: Boolean) {
            interfaceSettingsRepository.setOpenTranslatorOnTopOfApp(isChecked)
        }

    fun onCheckboxSaveNavigationStateClicked(isChecked: Boolean) {
        interfaceSettingsRepository.setSaveNavigationStateWhenSwitchingTab(isChecked)
    }

    class Factory(
        private val interfaceSettingsRepository: InterfaceSettingsRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(InterfaceSettingsViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                InterfaceSettingsViewModel(interfaceSettingsRepository) as T
            } else {
                throw IllegalArgumentException("unable to create ${InterfaceSettingsViewModel::class.qualifiedName}")
            }
        }

    }
}