package me.asddsajpgnoob.quizandvocabularyrx.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import io.reactivex.rxjava3.disposables.CompositeDisposable
import me.asddsajpgnoob.quizandvocabularyrx.ui.main.MainViewModel

abstract class BaseFragment<VB : ViewBinding> : Fragment() {

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    private var _disposable: CompositeDisposable? = null
    protected val disposable: CompositeDisposable get() = _disposable!!

    private var _viewDisposable: CompositeDisposable? = null
    protected val viewDisposable: CompositeDisposable get() = _viewDisposable!!

    protected abstract val mainViewModel: MainViewModel

    abstract fun getViewBinding(): VB

    abstract fun getOrientation(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _disposable = CompositeDisposable()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = getViewBinding()

        _viewDisposable = CompositeDisposable()

        mainViewModel.setOrientation(getOrientation())

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _viewDisposable!!.dispose()
        _viewDisposable = null
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        _disposable!!.dispose()
        _disposable = null
    }

    /**
     * @return true if navigated false otherwise
     * */
    protected fun navigateIfInDestination(
        @IdRes destinationId: Int,
        directions: NavDirections
    ): Boolean {
        val navController = findNavController()
        if (navController.currentDestination?.id == destinationId) {
            navController.navigate(directions)
            return true
        }
        return false
    }

    protected fun navigate(directions: NavDirections) {
        findNavController().navigate(directions)
    }

    protected fun popBackStackIfInDestination(@IdRes destinationId: Int): Boolean {
        val navController = findNavController()
        if (navController.currentDestination?.id == destinationId) {
            navController.popBackStack()
            return true
        }
        return false
    }

    protected fun popBackStack() {
        findNavController().popBackStack()
    }

    protected fun showToast(toast: String) {
        Toast.makeText(requireContext(), toast, Toast.LENGTH_SHORT).show()
    }

    protected fun showToast(@StringRes stringRes: Int) {
        Toast.makeText(requireContext(), stringRes, Toast.LENGTH_SHORT).show()
    }

    protected fun showLongToast(toast: String) {
        Toast.makeText(requireContext(), toast, Toast.LENGTH_LONG).show()
    }

    protected fun showLongToast(@StringRes stringRes: Int) {
        Toast.makeText(requireContext(), stringRes, Toast.LENGTH_LONG).show()
    }
}