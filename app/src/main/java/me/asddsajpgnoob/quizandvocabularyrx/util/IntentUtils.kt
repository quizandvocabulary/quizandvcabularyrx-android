package me.asddsajpgnoob.quizandvocabularyrxrx.util

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import me.asddsajpgnoob.quizandvocabularyrx.BuildConfig


object IntentUtils {

    fun getActionViewIntent(url: String): Intent {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        return intent
    }

    fun getAppSettingsIntent(): Intent {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
        intent.data = uri
        return intent
    }
}