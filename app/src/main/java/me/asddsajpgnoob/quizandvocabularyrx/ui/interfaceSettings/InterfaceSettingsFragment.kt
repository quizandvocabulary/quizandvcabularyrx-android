package me.asddsajpgnoob.quizandvocabularyrx.ui.interfaceSettings

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.databinding.FragmentInterfaceSettingsBinding
import me.asddsajpgnoob.quizandvocabularyrx.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabularyrx.ui.main.MainViewModel

@ExperimentalCoroutinesApi
class InterfaceSettingsFragment : BaseFragment<FragmentInterfaceSettingsBinding>() {

    private val viewModel: InterfaceSettingsViewModel by viewModels {
        InterfaceSettingsViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentInterfaceSettingsBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setOnClickListeners()

        collectDarkMode()
        collectDefaultTab()
        collectShowAudiosInManageScreen()
        collectShowExamplesInManageScreen()
        collectOpenTranslatorOnTopOfApp()
        collectSaveNavigationState()

        return view
    }

    @Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
    private fun collectDarkMode() {
        viewDisposable.add(
            viewModel.darkMode
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        DarkModePreferences.DARK -> {
                            binding.radioButtonDark.isChecked = true
                        }
                        DarkModePreferences.LIGHT -> {
                            binding.radioButtonLight.isChecked = true
                        }
                        DarkModePreferences.FOLLOW_SYSTEM -> {
                            binding.radioButtonDeviceTheme.isChecked = true
                        }
                    }
                }
        )
    }

    @Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
    private fun collectDefaultTab() {
        viewDisposable.add(
            viewModel.defaultTab
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        DefaultTabPreferences.QUIZ -> {
                            binding.radioButtonQuiz.isChecked = true
                        }
                        DefaultTabPreferences.VOCABULARY -> {
                            binding.radioButtonVocabulary.isChecked = true
                        }
                    }
                }
        )
    }

    private fun collectShowAudiosInManageScreen() {
        viewDisposable.add(
            viewModel.showAudiosInVocabularyScreen
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.checkBoxShowAudios.isChecked = it
                }
        )
    }

    private fun collectShowExamplesInManageScreen() {
        viewDisposable.add(
            viewModel.showExamplesInVocabularyScreen
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.checkBoxShowExamples.isChecked = it
                }
        )
    }

    private fun collectOpenTranslatorOnTopOfApp() {
        viewDisposable.add(
            viewModel.openTranslatorOnTopOfApp
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.checkBoxOpenTranslatorOnTopOfApp.isChecked = it
                }
        )
    }

    private fun collectSaveNavigationState() {
        viewDisposable.add(
            viewModel.saveNavigationStateWhenSwitchingTab
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.checkBoxSaveNavigationState.isChecked = it
                }
        )
    }

    private fun setOnClickListeners() {
        binding.apply {
            radioButtonDark.setOnClickListener {
                viewModel.onRadioButtonDarkClicked()
            }
            radioButtonLight.setOnClickListener {
                viewModel.onRadioButtonLightClicked()
            }
            radioButtonDeviceTheme.setOnClickListener {
                viewModel.onRadioButtonDeviceThemeClicked()
            }
            radioButtonVocabulary.setOnClickListener {
                viewModel.onRadioButtonVocabularyClicked()
            }
            radioButtonQuiz.setOnClickListener {
                viewModel.onRadioButtonQuizClicked()
            }
            checkBoxShowExamples.setOnClickListener {
                viewModel.onCheckboxShowExamplesClicked(checkBoxShowExamples.isChecked)
            }
            checkBoxShowAudios.setOnClickListener {
                viewModel.onCheckBoxShowAudiosClicked(checkBoxShowAudios.isChecked)
            }
            checkBoxOpenTranslatorOnTopOfApp.setOnClickListener {
                viewModel.onCheckBoxOpenTranslatorOnTopOfAppClicked(checkBoxOpenTranslatorOnTopOfApp.isChecked)
            }
            checkBoxSaveNavigationState.setOnClickListener {
                viewModel.onCheckboxSaveNavigationStateClicked(checkBoxSaveNavigationState.isChecked)
            }
        }
    }

}