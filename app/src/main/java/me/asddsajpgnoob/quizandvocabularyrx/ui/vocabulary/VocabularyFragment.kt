package me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary

import android.content.pm.ActivityInfo
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Bundle
import android.view.*
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.VocabularyCardRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.databinding.FragmentVocabularyBinding
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.AddEditVocabularyCardFragment
import me.asddsajpgnoob.quizandvocabularyrx.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabularyrx.ui.main.MainViewModel
import me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.adapter.VocabularyCardsAdapter
import me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.model.VocabularyEvent
import me.asddsajpgnoob.quizandvocabularyrx.util.BitmapUtils
import me.asddsajpgnoob.quizandvocabularyrx.util.DisplayMeasureUtils
import me.asddsajpgnoob.quizandvocabularyrx.util.recyclerView.RecyclerViewShowHideScrollListener

@ExperimentalCoroutinesApi
class VocabularyFragment : BaseFragment<FragmentVocabularyBinding>(),
    VocabularyCardsAdapter.VocabularyCardAdapterConnector {

    companion object {
        private const val CURRENT_DESTINATION_ID = R.id.navigation_vocabulary
    }

    private val cardsAdapter = VocabularyCardsAdapter(this)

    private lateinit var cardsLayoutManager: LinearLayoutManager

    private val viewModel: VocabularyViewModel by viewModels {
        VocabularyViewModel.Factory(
            InterfaceSettingsRepositoryImpl,
            VocabularyCardRepositoryImpl,
            requireContext().getExternalFilesDir(null)!!,
        )
    }

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentVocabularyBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setupRecyclerViews()
        setOnClickListeners()

        collectCardAndPlaybackInfo()
        collectListItems()

        collectEvents()

        return view
    }

    private fun setupRecyclerViews() {
        binding.apply {
            recyclerViewCards.setHasFixedSize(true)
            cardsLayoutManager = binding.recyclerViewCards.layoutManager as LinearLayoutManager

            recyclerViewCards.addOnScrollListener(object : RecyclerViewShowHideScrollListener() {

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy >= 0) {
                        val itemCount = cardsLayoutManager.itemCount
                        val lastVisibleItemPos = cardsLayoutManager.findLastVisibleItemPosition()
                        if (lastVisibleItemPos == itemCount - 1) {
                            viewModel.onListBottomReached()
                        }
                    }


                    super.onScrolled(recyclerView, dx, dy)
                }

                override fun show() {
                    binding.floatingButtonAdd.animate()
                        .translationY(0f)
                        .setInterpolator(DecelerateInterpolator(2f))
                        .start()
                }

                override fun hide() {
                    binding.floatingButtonAdd.animate()
                        .translationY(binding.floatingButtonAdd.height + 60f)
                        .setInterpolator(AccelerateInterpolator(2f))
                        .start()
                }
            })

            val bitmap = BitmapUtils.getBitmapFromVectorDrawable(
                requireContext(),
                R.drawable.ic_delete_forever_white_24
            )
            val bitmapSize = DisplayMeasureUtils.dpToPx(requireContext(), 24f)
            val cornerRadius = resources.getDimension(R.dimen.main_card_view_corner_radius)

            val itemTouchHelperCallback = object :
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    viewModel.onItemSwiped(cardsAdapter.getItem(viewHolder.adapterPosition))
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {
                    val itemView = viewHolder.itemView
                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                        val backgroundPaint = Paint()
                        backgroundPaint.color =
                            ContextCompat.getColor(requireContext(), R.color.red)
                        c.drawRoundRect(
                            itemView.left.toFloat(),
                            itemView.top.toFloat(),
                            itemView.right.toFloat(),
                            itemView.bottom.toFloat(),
                            cornerRadius,
                            cornerRadius,
                            backgroundPaint
                        )
                        val bitmapPaint = Paint()
                        bitmapPaint.color = ContextCompat.getColor(requireContext(), R.color.white)
                        if (dX > 0) {
                            c.drawBitmap(
                                bitmap,
                                itemView.left.toFloat() + bitmapSize / 2f,
                                itemView.bottom.toFloat() - (itemView.bottom - itemView.top) / 2f - bitmapSize / 2f,
                                bitmapPaint
                            )
                        } else if (dX < 0) {
                            c.drawBitmap(
                                bitmap,
                                itemView.right.toFloat() - bitmapSize - bitmapSize / 2f,
                                itemView.bottom.toFloat() - (itemView.bottom - itemView.top) / 2f - bitmapSize / 2f,
                                bitmapPaint
                            )
                        }
                    }
                    super.onChildDraw(
                        c,
                        recyclerView,
                        viewHolder,
                        dX,
                        dY,
                        actionState,
                        isCurrentlyActive
                    )
                }

                override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float {
                    return 0.7f
                }

            }
            val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
            itemTouchHelper.attachToRecyclerView(recyclerViewCards)

            recyclerViewCards.adapter = cardsAdapter
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            floatingButtonAdd.setOnClickListener {
                viewModel.onFloatingButtonAddClicked()
            }
        }
    }

    private fun collectCardAndPlaybackInfo() {
        viewDisposable.add(
            viewModel.cardAndPlaybackInfo
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    cardsAdapter.updatePlaybackInfo(it.value)
                }
        )
    }

    private fun collectListItems() {
        viewDisposable.add(
            viewModel.listItems
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    cardsAdapter.submitListData(it)
                }
        )
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onStart(owner: LifecycleOwner) {
                viewModel.events.subscribe {
                    viewDisposable.add(
                        Single.just(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { event ->
                                when (event) {
                                    VocabularyEvent.NavigateToAddVocabularyCardFragment -> {
                                        val action =
                                            VocabularyFragmentDirections.actionNavigationVocabularyToAddEditVocabularyCardFragment(
                                                null
                                            )
                                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                                    }
                                    is VocabularyEvent.NavigateToEditVocabularyCardFragment -> {
                                        val action =
                                            VocabularyFragmentDirections.actionNavigationVocabularyToAddEditVocabularyCardFragment(
                                                event.card
                                            )
                                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                                    }
                                    is VocabularyEvent.ShowToast -> {
                                        showToast(event.stringRes)
                                    }
                                }
                            }
                    )
                }
            }

            override fun onStop(owner: LifecycleOwner) {
                viewModel.events.unsubscribe()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_manage, menu)

        val searchView = menu.findItem(R.id.item_search).actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.onSearchQueryChanged(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.onSearchQueryChanged(newText)
                return true
            }
        })

        val query = viewModel.getCurrentSearchQuery().value
        if (!query.isNullOrBlank()) {
            searchView.setQuery(query, false)
            searchView.isIconified = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.item_settings -> {
            val action = VocabularyFragmentDirections.actionNavigationVocabularyToSettingsFragment()
            navigateIfInDestination(CURRENT_DESTINATION_ID, action)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onCardClicked(card: VocabularyCard) {
        viewModel.onCardClicked(card)
    }

    override fun onPlayPauseClicked(card: VocabularyCard, fileName: String) {
        viewModel.onPlayPauseClicked(card, fileName)
    }

}