package me.asddsajpgnoob.quizandvocabularyrx.ui.quiz.model

enum class DragDirection {
    RIGHT,
    LEFT,
    NONE;
}