package me.asddsajpgnoob.quizandvocabularyrx.data.repository

import io.reactivex.rxjava3.core.Flowable
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DefaultTabPreferences

interface InterfaceSettingsRepository {

    val darkMode: Flowable<DarkModePreferences>

    val defaultTab: Flowable<DefaultTabPreferences>

    val showAudiosInVocabularyScreen: Flowable<Boolean>

    val showExamplesInVocabularyScreen: Flowable<Boolean>

    val openTranslatorOnTopOfApp: Flowable<Boolean>

    val saveNavigationStateWhenSwitchingTab: Flowable<Boolean>

    fun setDarkMode(darkMode: DarkModePreferences)

    fun setDefaultTab(defaultTab: DefaultTabPreferences)

    fun setShowAudiosInVocabularyScreen(show: Boolean)

    fun setExamplesAudiosInVocabularyScreen(show: Boolean)

    fun setOpenTranslatorOnTopOfApp(open: Boolean)

    fun setSaveNavigationStateWhenSwitchingTab(save: Boolean)
}