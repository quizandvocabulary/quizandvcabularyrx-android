package me.asddsajpgnoob.quizandvocabularyrx.ui.quiz.model

import androidx.annotation.StringRes
import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard

sealed interface QuizEvent {
    data class NavigateToAddEditVocabularyCardScreen(val vocabularyCard: VocabularyCard) : QuizEvent

    object NavigateToTouchHintDialog : QuizEvent

    object NavigateToSwipeHintDialog : QuizEvent

    data class ShowToast(@StringRes val stringRes: Int) : QuizEvent
}