package me.asddsajpgnoob.quizandvocabularyrx.util

data class KOptional<T>(val value: T?) {
    companion object {
        fun <T> empty() = KOptional<T>(null)

        fun <T> wrap(get: () -> T?) = KOptional(get.invoke())
    }

    inline fun getOr(ifEmpty: () -> T): T {
        return value ?: ifEmpty.invoke()
    }
}

fun <T> T?.kOptional(): KOptional<T> = KOptional(this)