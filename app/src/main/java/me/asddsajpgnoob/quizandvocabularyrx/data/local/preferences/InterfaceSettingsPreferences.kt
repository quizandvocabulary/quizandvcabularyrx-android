package me.asddsajpgnoob.quizandvocabularyrx.data.local.preferences

import android.content.Context
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.rxjava3.rxPreferencesDataStore
import androidx.datastore.rxjava3.RxDataStore
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.App
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabularyrx.util.rx.emitEmptyIfIOException

@ExperimentalCoroutinesApi
object InterfaceSettingsPreferences {

    private object InterfaceSettingsKeys {
        val DARK_MODE = stringPreferencesKey("darkMode")
        val DEFAULT_TAB = stringPreferencesKey("defaultTab")
        val SHOW_AUDIOS_IN_VOCABULARY_SCREEN = booleanPreferencesKey("showAudiosInVocabularyScreen")
        val SHOW_EXAMPLES_IN_VOCABULARY_SCREEN =
            booleanPreferencesKey("showExamplesInVocabularyScreen")
        val OPEN_TRANSLATOR_ON_TOP_OF_APP = booleanPreferencesKey("openTranslatorOnTopOfApp")
        val SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB =
            booleanPreferencesKey("saveNavigationStateWhenSwitchingTab")
    }

    private val Context.interfaceSettingsDataStore: RxDataStore<Preferences> by rxPreferencesDataStore(
        name = "interface_settings_preferences"
    )

    val DEFAULT_DARK_MODE = DarkModePreferences.FOLLOW_SYSTEM
    val DEFAULT_DEFAULT_TAB = DefaultTabPreferences.QUIZ
    const val DEFAULT_SHOW_AUDIOS_IN_VOCABULARY_SCREEN = false
    const val DEFAULT_SHOW_EXAMPLES_IN_VOCABULARY_SCREEN = true
    const val DEFAULT_OPEN_TRANSLATOR_ON_TOP_OF_APP = false
    const val DEFAULT_SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB = true

    val darkMode: Flowable<DarkModePreferences> = App.INSTANCE.interfaceSettingsDataStore.data()
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[InterfaceSettingsKeys.DARK_MODE]?.let {
                DarkModePreferences.findByPreferenceName(it)
            } ?: DEFAULT_DARK_MODE
        }

    val defaultTab: Flowable<DefaultTabPreferences> =
        App.INSTANCE.interfaceSettingsDataStore.data()
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.DEFAULT_TAB]?.let {
                    DefaultTabPreferences.findByPreferenceName(it)
                } ?: DEFAULT_DEFAULT_TAB
            }

    val showAudiosInVocabularyScreen: Flowable<Boolean> =
        App.INSTANCE.interfaceSettingsDataStore.data()
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.SHOW_AUDIOS_IN_VOCABULARY_SCREEN]
                    ?: DEFAULT_SHOW_AUDIOS_IN_VOCABULARY_SCREEN
            }

    val showExamplesInVocabularyScreen: Flowable<Boolean> =
        App.INSTANCE.interfaceSettingsDataStore.data()
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.SHOW_EXAMPLES_IN_VOCABULARY_SCREEN]
                    ?: DEFAULT_SHOW_EXAMPLES_IN_VOCABULARY_SCREEN
            }

    val openTranslatorOnTopOfApp: Flowable<Boolean> = App.INSTANCE.interfaceSettingsDataStore.data()
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[InterfaceSettingsKeys.OPEN_TRANSLATOR_ON_TOP_OF_APP]
                ?: DEFAULT_OPEN_TRANSLATOR_ON_TOP_OF_APP

        }

    val saveNavigationStateWhenSwitchingTab: Flowable<Boolean> =
        App.INSTANCE.interfaceSettingsDataStore.data()
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[InterfaceSettingsKeys.SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB]
                    ?: DEFAULT_SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB
            }

    fun setDarkMode(darkMode: DarkModePreferences) {
        App.INSTANCE.interfaceSettingsDataStore.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[InterfaceSettingsKeys.DARK_MODE] = darkMode.preferenceName
                it.onSuccess(prefs)
            }
        }
    }

    fun setDefaultTab(defaultTab: DefaultTabPreferences) {
        App.INSTANCE.interfaceSettingsDataStore.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[InterfaceSettingsKeys.DEFAULT_TAB] = defaultTab.preferenceName
                it.onSuccess(prefs)
            }
        }
    }

    fun setShowAudiosInVocabularyScreen(show: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[InterfaceSettingsKeys.SHOW_AUDIOS_IN_VOCABULARY_SCREEN] = show
                it.onSuccess(prefs)
            }
        }
    }

    fun setExamplesAudiosInVocabularyScreen(show: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[InterfaceSettingsKeys.SHOW_EXAMPLES_IN_VOCABULARY_SCREEN] = show
                it.onSuccess(prefs)
            }
        }
    }

    fun setOpenTranslatorOnTopOfApp(open: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[InterfaceSettingsKeys.OPEN_TRANSLATOR_ON_TOP_OF_APP] = open
                it.onSuccess(prefs)
            }
        }
    }

    fun setSaveNavigationStateWhenSwitchingTab(save: Boolean) {
        App.INSTANCE.interfaceSettingsDataStore.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[InterfaceSettingsKeys.SAVE_NAVIGATION_STATE_WHEN_SWITCHING_TAB] = save
                it.onSuccess(prefs)
            }
        }
    }
}