package me.asddsajpgnoob.quizandvocabularyrx.ui.settings

import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.processors.PublishProcessor
import me.asddsajpgnoob.quizandvocabularyrx.ui.settings.model.SettingsEvent
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventHandler
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventReceiver

class SettingsViewModel : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _events = EventHandler<SettingsEvent>()
    val events: EventReceiver<SettingsEvent> get() = _events

    fun onInterfaceLabelClick() {
        _events.onNext(SettingsEvent.NavigateToInterfaceSettingsFragment)
    }

    fun onQuizClicked() {
        _events.onNext(SettingsEvent.NavigateToQuizSettingsFragment)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}