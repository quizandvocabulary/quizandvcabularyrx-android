package me.asddsajpgnoob.quizandvocabularyrx.ui.quiz

import android.media.MediaPlayer
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import kotlinx.coroutines.Job
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabularyrx.data.model.room.VocabularyCardEntity
import me.asddsajpgnoob.quizandvocabularyrx.data.modifiers.VocabularyCardModifiers.domainToEntity
import me.asddsajpgnoob.quizandvocabularyrx.data.modifiers.VocabularyCardModifiers.entityListToDomainList
import me.asddsajpgnoob.quizandvocabularyrx.data.modifiers.VocabularyCardModifiers.entityToDomain
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.OnTimeActionRepository
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.QuizSettingsRepository
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.VocabularyCardRepository
import me.asddsajpgnoob.quizandvocabularyrx.eventBus.EventBus
import me.asddsajpgnoob.quizandvocabularyrx.eventBus.model.VocabularyCardEvent
import me.asddsajpgnoob.quizandvocabularyrx.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabularyrx.ui.dialog.genericAlertDialog.GenericAlertDialog
import me.asddsajpgnoob.quizandvocabularyrx.ui.quiz.model.GenericAlertDialogState
import me.asddsajpgnoob.quizandvocabularyrx.ui.quiz.model.QuizEvent
import me.asddsajpgnoob.quizandvocabularyrx.ui.quiz.model.QuizState
import me.asddsajpgnoob.quizandvocabularyrx.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabularyrx.util.Constants
import me.asddsajpgnoob.quizandvocabularyrx.util.KOptional
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventHandler
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventReceiver
import me.asddsajpgnoob.quizandvocabularyrx.util.kOptional
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

class QuizViewModel(
    private val vocabularyCardRepository: VocabularyCardRepository,
    private val onTimeActionRepository: OnTimeActionRepository,
    quizSettingsRepository: QuizSettingsRepository,
    private val externalFilesDir: File
) : ViewModel() {

    companion object {
        private const val LIMIT = 20
    }

    private val disposable = CompositeDisposable()

    private val _playbackInfo: BehaviorSubject<KOptional<PlaybackInfo>> =
        BehaviorSubject.createDefault(KOptional.empty())
    val playbackInfo: Observable<KOptional<PlaybackInfo>> get() = _playbackInfo

    private val _quizState: BehaviorSubject<QuizState> =
        BehaviorSubject.createDefault(QuizState.Loading)
    val quizState: Observable<QuizState> get() = _quizState

    private val showQuizTouchHint = onTimeActionRepository.showQuizTouchHint

    private val showQuizSwipeDirectionHint = onTimeActionRepository.showQuizSwipeDirectionHint

    private val quizMode = quizSettingsRepository.quizMode

    private val _events = EventHandler<QuizEvent>()
    val events: EventReceiver<QuizEvent> get() = _events

    private val passedCardIds = mutableListOf<Long>()
    private val availableCards = mutableListOf<VocabularyCard>()

    private var offset = 0

    private var mediaPlayerPositionJob: Job? = null
    private var mediaPlayer: MediaPlayer? = null

    private var genericAlertDialogState: GenericAlertDialogState =
        GenericAlertDialogState.NotShowing

    private var currentQuizMode = QuizModePreferences.RANDOM

    init {
        disposable.add(
            quizMode.subscribe {
                currentQuizMode = it
                refreshQuiz()
            }
        )
        disposable.add(
            EventBus.vocabularyCardEventBus.subscribe { event ->
                when(event) {
                    is VocabularyCardEvent.CardCreated -> {
                        onCardCreated(event.card)
                    }
                    is VocabularyCardEvent.CardDeleted -> {
                        onCardDeleted(event.card)
                    }
                    is VocabularyCardEvent.CardEdited -> {
                        onCardEdited(event.editedCard)
                    }
                }
            }
        )
    }

    private fun refreshQuiz() {
        passedCardIds.clear()
        availableCards.clear()
        offset = 0
        when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                loadNextRandomQuizCard()
            }
            QuizModePreferences.FIRST_LESS_GUESSED,
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
            QuizModePreferences.NEWEST_FIRST,
            QuizModePreferences.OLDEST_FIRST -> {
                loadPackOfCardsAndShowCard()
            }
        }
    }

    private fun loadPackOfCardsAndShowCard() {
        _quizState.onNext(QuizState.Loading)
        val cardsSingle: Single<List<VocabularyCardEntity>> = when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                throw RuntimeException("can't be random mode")
            }
            QuizModePreferences.FIRST_LESS_GUESSED -> {
                vocabularyCardRepository.findLessGuessedVocabularyCards(offset, LIMIT)
            }
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM -> {
                vocabularyCardRepository.findLessGuessedVocabularyCards(offset, LIMIT)
            }
            QuizModePreferences.NEWEST_FIRST -> {
                vocabularyCardRepository.findNewestVocabularyCards(offset, LIMIT)
            }
            QuizModePreferences.OLDEST_FIRST -> {
                vocabularyCardRepository.findOldestVocabularyCards(offset, LIMIT)
            }
        }
        disposable.add(
            cardsSingle.subscribe { cards ->
                if (cards.isEmpty()) {
                    if (offset == 0) {
                        _quizState.onNext(QuizState.NothingToShow)
                    } else {
                        _quizState.onNext(QuizState.AllPassed)
                    }
                    return@subscribe
                }
                availableCards.clear()
                availableCards.addAll(
                    cards.entityListToDomainList()
                )
                offset += LIMIT
                showFromAvailableCardsOrLoadPack()
            }
        )
    }

    private fun showFromAvailableCardsOrLoadPack() {
        val pickRandom = when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                throw RuntimeException("can't be random mode")
            }
            QuizModePreferences.FIRST_LESS_GUESSED -> {
                false
            }
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM -> {
                true
            }
            QuizModePreferences.NEWEST_FIRST -> {
                false
            }
            QuizModePreferences.OLDEST_FIRST -> {
                false
            }
        }
        val nextCard = if (pickRandom) {
            availableCards.randomOrNull()
        } else {
            availableCards.firstOrNull()
        }
        if (nextCard != null) {
            sendShowTouchHintDialogEventIfNeeded()
            _quizState.onNext(QuizState.Show(nextCard, true))
            availableCards.remove(nextCard)
        } else {
            loadPackOfCardsAndShowCard()
        }
    }

    fun onGenericAlertDialogResult(result: GenericAlertDialog.GenericAlertDialogResult) {
        when (result) {
            GenericAlertDialog.GenericAlertDialogResult.POSITIVE -> {
                when (genericAlertDialogState) {
                    GenericAlertDialogState.ShowingTouchHintDialog -> {
                        onTimeActionRepository.setShowQuizTouchHint(false)
                    }
                    GenericAlertDialogState.ShowingSwipeHintDialog -> {
                        onTimeActionRepository.setShowQuizSwipeDirectionHint(false)
                    }
                    GenericAlertDialogState.NotShowing -> {
                        // do not nothing
                    }
                }
            }
            GenericAlertDialog.GenericAlertDialogResult.NEGATIVE -> {
                throw RuntimeException("not handled such case")
            }
            GenericAlertDialog.GenericAlertDialogResult.NEUTRAL -> {
                throw RuntimeException("not handled such case")
            }
            GenericAlertDialog.GenericAlertDialogResult.CANCELED -> {
                // nothing to do
            }
        }
        genericAlertDialogState = GenericAlertDialogState.NotShowing
    }

    fun onButtonRestartClicked() {
        refreshQuiz()
    }

    fun onQuizCardDisappeared(guessed: Boolean) {
        releaseMediaPlayer()
        _quizState.value.let {
            if (it is QuizState.Show) {
                val oldCard = it.vocabularyCard
                val updatedCard = if (guessed) {
                    it.vocabularyCard.copy(
                        guessedCount = it.vocabularyCard.guessedCount + 1,
                        updatedAt = Date()
                    )
                } else {
                    it.vocabularyCard.copy(
                        notGuessedCount = it.vocabularyCard.notGuessedCount + 1,
                        updatedAt = Date()
                    )
                }
                updateQuizCardInDb(oldCard, updatedCard)
                if (currentQuizMode == QuizModePreferences.RANDOM) {
                    passedCardIds.add(it.vocabularyCard.id)
                }
            }
        }
        when (currentQuizMode) {
            QuizModePreferences.RANDOM -> {
                loadNextRandomQuizCard()
            }
            QuizModePreferences.FIRST_LESS_GUESSED,
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
            QuizModePreferences.NEWEST_FIRST,
            QuizModePreferences.OLDEST_FIRST -> {
                showFromAvailableCardsOrLoadPack()
            }
        }
    }

    fun onShowMoreClicked() {
        _quizState.value.let {
            if (it is QuizState.Show) {
                _events.onNext(QuizEvent.NavigateToAddEditVocabularyCardScreen(it.vocabularyCard))
            }
        }
    }

    fun onQuizCardAnimationEnd(currentFirstVisible: Boolean) {
        _quizState.value.let {
            if (it is QuizState.Show) {
                _quizState.onNext(
                    it.copy(
                        isShowingFirst = currentFirstVisible
                    )
                )
                sendShowSwipeHintDialogEventIfNeeded()
            }
        }
    }

    fun onQuizCardShowInQuizCheckedChange(checked: Boolean) {
        _quizState.value.let {
            if (it is QuizState.Show) {
                _quizState.onNext(
                    it.copy(
                        vocabularyCard = it.vocabularyCard.copy(
                            showInQuiz = checked
                        )
                    )
                )
            }
        }
    }

    fun onPlayPauseClicked(fileName: String) {
        val playingInfo = _playbackInfo.value!!
        if (playingInfo.value != null && playingInfo.value.fileName != fileName) {
            releaseMediaPlayer()
        }
        if (!isMediaPlayerPlaying()) {
            if (mediaPlayer == null) {
                val audioFile =
                    File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}/${fileName}")
                if (!audioFile.exists()) {
                    showToast(R.string.Audio_file_not_found)
                    return
                }
                initMediaPlayer(audioFile)
            }
            startMediaPlayer()
            return
        }
        pauseMediaPlayer()
    }

    fun getCurrentPlayBackInfo(): KOptional<PlaybackInfo> {
        return _playbackInfo.value!!
    }

    private fun showToast(@StringRes stringRes: Int) {
        _events.onNext(QuizEvent.ShowToast(stringRes))
    }

    private fun sendShowSwipeHintDialogEventIfNeeded() {
        _quizState.value.let {
            if (it is QuizState.Show) {
                disposable.add(
                    showQuizSwipeDirectionHint.firstElement().subscribe { showHint ->
                        if (!it.isShowingFirst && showHint) {
                            genericAlertDialogState = GenericAlertDialogState.ShowingSwipeHintDialog
                            _events.onNext(QuizEvent.NavigateToSwipeHintDialog)
                        }
                    }
                )
            }
        }
    }

    private fun sendShowTouchHintDialogEventIfNeeded() {
        disposable.add(
            showQuizTouchHint.firstElement()
                .subscribe { showHint ->
                    if (showHint) {
                        genericAlertDialogState = GenericAlertDialogState.ShowingTouchHintDialog
                        _events.onNext(QuizEvent.NavigateToTouchHintDialog)
                    }
                }
        )
    }

    private fun updateQuizCardInDb(oldCard: VocabularyCard, card: VocabularyCard) {
        vocabularyCardRepository.update(card.domainToEntity()).subscribe {
            EventBus.submitVocabularyEvent(VocabularyCardEvent.CardEdited(oldCard, card))
        }
    }

    private fun loadNextRandomQuizCard() {
        _quizState.onNext(QuizState.Loading)
        val cardMaybe: Maybe<VocabularyCardEntity> =
            vocabularyCardRepository.findRandomVocabularyCard(passedCardIds)

        disposable.add(
            cardMaybe.isEmpty
                .subscribe { empty ->
                    var nextQuizState: QuizState
                    if (empty) {
                        nextQuizState = if (passedCardIds.isEmpty()) {
                            QuizState.NothingToShow
                        } else {
                            QuizState.AllPassed
                        }
                        _quizState.onNext(nextQuizState)
                    } else {
                        disposable.add(
                            cardMaybe.subscribe { card ->
                                sendShowTouchHintDialogEventIfNeeded()
                                nextQuizState = QuizState.Show(card.entityToDomain(), true)
                                _quizState.onNext(nextQuizState)
                            }
                        )
                    }
                }
        )
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    private fun releaseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
        _playbackInfo.onNext(KOptional.empty())
    }

    private fun pauseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer!!.pause()
        _playbackInfo.onNext(
            _playbackInfo.value!!.value!!.copy(
                playing = false
            ).kOptional()
        )
    }

    private fun initMediaPlayer(audioFile: File) {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        _playbackInfo.onNext(
            PlaybackInfo(
                false,
                audioFile.name,
                mediaPlayer!!.duration,
                0
            ).kOptional()
        )
        mediaPlayer!!.setOnCompletionListener {
            releaseMediaPlayer()
            _playbackInfo.onNext(KOptional.empty())
        }
    }

    private fun startMediaPlayer() {
        mediaPlayer!!.start()
        val interval = Observable.interval(0L, 20L, TimeUnit.MILLISECONDS)
            .takeWhile {
                isMediaPlayerPlaying()
            }
        disposable.add(
            interval.subscribe {
                _playbackInfo.onNext(
                    _playbackInfo.value!!.value!!.copy(
                        playing = true,
                        playingPosition = mediaPlayer!!.currentPosition
                    ).kOptional()
                )
            }
        )
    }

    private fun onCardEdited(editedCard: VocabularyCard) {
        val currentQuizState = _quizState.value
        if (currentQuizState is QuizState.Show) {
            if (currentQuizState.vocabularyCard.id == editedCard.id) {
                _quizState.onNext(QuizState.Show(editedCard, currentQuizState.isShowingFirst))
            }
        }
    }

    private fun onCardDeleted(deletedCard: VocabularyCard) {
        _quizState.value.let {
            if (it is QuizState.Show) {
                if (it.vocabularyCard == deletedCard) {
                    when (currentQuizMode) {
                        QuizModePreferences.RANDOM -> {
                            loadNextRandomQuizCard()
                        }
                        QuizModePreferences.FIRST_LESS_GUESSED,
                        QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
                        QuizModePreferences.NEWEST_FIRST,
                        QuizModePreferences.OLDEST_FIRST -> {
                            offset--
                            showFromAvailableCardsOrLoadPack()
                        }
                    }
                }
            }
        }
    }

    private fun onCardCreated(createdCard: VocabularyCard) {
        when(currentQuizMode) {
            QuizModePreferences.RANDOM,
            QuizModePreferences.FIRST_LESS_GUESSED,
            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM,
            QuizModePreferences.OLDEST_FIRST -> {
                _quizState.value.let {
                    if (it is QuizState.NothingToShow) {
                        offset++
                        _quizState.onNext(QuizState.Show(createdCard, true))
                    }
                }
            }
            QuizModePreferences.NEWEST_FIRST -> {
                _quizState.value.let {
                    offset++
                    if (it is QuizState.NothingToShow) {
                        _quizState.onNext(QuizState.Show(createdCard, true))
                    } else {
                        availableCards.add(0, createdCard)
                    }
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
        disposable.dispose()
    }

    class Factory(
        private val vocabularyCardRepository: VocabularyCardRepository,
        private val onTimeActionRepository: OnTimeActionRepository,
        private val quizSettingsRepository: QuizSettingsRepository,
        private val externalFilesDir: File
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(QuizViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                QuizViewModel(
                    vocabularyCardRepository,
                    onTimeActionRepository,
                    quizSettingsRepository,
                    externalFilesDir
                ) as T
            } else {
                throw IllegalArgumentException("unable to create ${QuizViewModel::class.qualifiedName}")
            }
        }
    }
}