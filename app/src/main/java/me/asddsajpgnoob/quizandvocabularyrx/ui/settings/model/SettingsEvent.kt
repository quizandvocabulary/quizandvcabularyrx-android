package me.asddsajpgnoob.quizandvocabularyrx.ui.settings.model

sealed interface SettingsEvent {

    object NavigateToInterfaceSettingsFragment : SettingsEvent

    object NavigateToQuizSettingsFragment: SettingsEvent
}