package me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.model

import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabularyrx.ui.common.model.PlaybackInfo

data class CardAndPlaybackInfo(
    val playbackInfo: PlaybackInfo,
    val card: VocabularyCard
)