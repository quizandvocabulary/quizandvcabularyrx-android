package me.asddsajpgnoob.quizandvocabularyrx.ui.quiz.model

sealed interface GenericAlertDialogState {

    object ShowingTouchHintDialog : GenericAlertDialogState

    object ShowingSwipeHintDialog : GenericAlertDialogState

    object NotShowing : GenericAlertDialogState

}