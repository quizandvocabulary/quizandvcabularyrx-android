package me.asddsajpgnoob.quizandvocabularyrx.data.local.preferences

import android.content.Context
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.rxjava3.rxPreferencesDataStore
import androidx.datastore.rxjava3.RxDataStore
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.App
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.util.rx.emitEmptyIfIOException

@ExperimentalCoroutinesApi
object QuizSettingsPreferences {

    object QuizSettingsKeys {
        val QUIZ_MODE = stringPreferencesKey("quizMode")
    }

    private val Context.quizSettingsDataStore: RxDataStore<Preferences> by rxPreferencesDataStore(
        name = "quiz_settings_preferences"
    )

    val DEFAULT_QUIZ_MODE = QuizModePreferences.RANDOM

    val quizMode: Flowable<QuizModePreferences> = App.INSTANCE.quizSettingsDataStore.data()
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[QuizSettingsKeys.QUIZ_MODE]?.let {
                QuizModePreferences.findByPreferenceName(it)
            } ?: DEFAULT_QUIZ_MODE
        }

    fun setQuizMode(quizMode: QuizModePreferences) {
        App.INSTANCE.quizSettingsDataStore.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[QuizSettingsKeys.QUIZ_MODE] = quizMode.preferenceName
                it.onSuccess(prefs)
            }
        }
    }

}