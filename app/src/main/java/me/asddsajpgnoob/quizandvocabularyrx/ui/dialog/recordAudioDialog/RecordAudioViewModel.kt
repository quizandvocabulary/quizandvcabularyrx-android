package me.asddsajpgnoob.quizandvocabularyrx.ui.dialog.recordAudioDialog

import android.content.Context
import android.media.MediaPlayer
import android.media.MediaRecorder
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import kotlinx.coroutines.Job
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.ui.dialog.recordAudioDialog.model.AudioViewState
import me.asddsajpgnoob.quizandvocabularyrx.ui.dialog.recordAudioDialog.model.MediaPlayerProgress
import me.asddsajpgnoob.quizandvocabularyrx.ui.dialog.recordAudioDialog.model.RecordAudioEvent
import me.asddsajpgnoob.quizandvocabularyrx.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabularyrx.util.AudioRecorder
import me.asddsajpgnoob.quizandvocabularyrx.util.Constants
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventHandler
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventReceiver
import java.io.File
import java.util.concurrent.TimeUnit

class RecordAudioViewModel : ViewModel() {

    private val disposable = CompositeDisposable()

    private val viewStateMutable: BehaviorSubject<AudioViewState> =
        BehaviorSubject.createDefault(AudioViewState.Initial)
    val viewState: Observable<AudioViewState> get() = viewStateMutable

    private val recordingTimeMutable: BehaviorSubject<Long> = BehaviorSubject.createDefault(0L)
    val recordingTime: Observable<Long> get() = recordingTimeMutable

    private val mediaPlayerProgressMutable: BehaviorSubject<MediaPlayerProgress> =
        BehaviorSubject.createDefault(MediaPlayerProgress(0, 0))
    val mediaPlayerProgress: Observable<MediaPlayerProgress> get() = mediaPlayerProgressMutable

    private val _events = EventHandler<RecordAudioEvent>()
    val events: EventReceiver<RecordAudioEvent> get() = _events

    private var mediaPlayerPositionJob: Job? = null
    private var mediaPlayer: MediaPlayer? = null

    private var audioRecorder: AudioRecorder? = null

    private var shouldDeleteAudioInExit = true

    private var recordAudioPermissionGranted = false

    init {
        _events.onNext(RecordAudioEvent.RequestRecordAudioPermission)
    }

    fun onRecordAudioPermissionGranted() {
        recordAudioPermissionGranted = true
    }

    fun onImageButtonMicHold(context: Context) {
        if (recordAudioPermissionGranted) {
            if (audioRecorder == null) {
                initAudioAacRecorder(context)
            }
            ifNotRecordingStartIt()
            return
        }
        _events.onNext(RecordAudioEvent.RequestRecordAudioPermission)
    }

    fun onImageButtonMicRelease() {
        ifRecordingStopIt()
    }

    fun onResetAudioClicked() {
        resetAudio()
    }

    fun onPlayPauseClicked() {
        if (isMediaPlayerPlaying()) {
            pauseMediaPlayer()
            return
        }

        startMediaPlayer()
    }

    fun onSubmitButtonClicked() {
        shouldDeleteAudioInExit = false
        if (audioRecorder?.isRecording == true) {
            _events.onNext(RecordAudioEvent.ShowToast(R.string.Still_recording))
            return
        }
        val audioFile = audioRecorder?.outputPath?.let {
            File(it)
        }
        if (audioFile == null) {
            _events.onNext(RecordAudioEvent.ShowToast(R.string.No_recorded_audio))
            return
        }
        if (isMediaPlayerPlaying()) {
            pauseMediaPlayer()
        }
        _events.onNext(RecordAudioEvent.PopBackStackWithResult(audioFile))
    }

    fun onCancelButtonClicked() {
        if (audioRecorder?.isRecording == true) {
            ifRecordingStopIt()
        }
        if (isMediaPlayerPlaying()) {
            pauseMediaPlayer()
        }
        _events.onNext(RecordAudioEvent.PopBackStack)
    }

    private fun ifNotRecordingStartIt() {
        if (!audioRecorder!!.isRecording) {
            audioRecorder!!.removeLastOutput()
            audioRecorder!!.startRecording()
            viewStateMutable.onNext(AudioViewState.Recording)
            _events.onNext(RecordAudioEvent.Vibrate)
        }
    }

    private fun ifRecordingStopIt() {
        if (audioRecorder?.isRecording == true) {
            var successFulStop = true
            val interval = Observable.interval(0L, 100L, TimeUnit.MILLISECONDS)
                .takeWhile {
                    audioRecorder?.isRecording == true
                }
            disposable.add(
                interval.subscribe {
                    if (recordingTimeMutable.value!! > 499L) {
                        audioRecorder!!.stopRecording()
                    } else {
                        successFulStop = false
                    }
                }
            )
            if (successFulStop) {
                initMediaPlayer(File(audioRecorder!!.outputPath!!))
                viewStateMutable.onNext(AudioViewState.RecordingCompleted(mediaPlayer!!.duration))
                _events.onNext(RecordAudioEvent.Vibrate)
            } else {
                viewStateMutable.onNext(AudioViewState.Initial)
                _events.onNext(RecordAudioEvent.ShowToast(R.string.Recording_must_be_at_least_half_second))
            }
        }
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    private fun startMediaPlayer() {
        if (viewStateMutable.value is AudioViewState.RecordingCompleted) {
            _events.onNext(RecordAudioEvent.Vibrate)
        }
        viewStateMutable.onNext(
            AudioViewState.Playing(
                mediaPlayer!!.duration,
                mediaPlayer!!.currentPosition
            )
        )
        mediaPlayer!!.start()
        val interval = Observable.interval(0L, 20L, TimeUnit.MILLISECONDS)
            .takeWhile {
                isMediaPlayerPlaying()
            }
        disposable.add(
            interval.subscribe {
                mediaPlayerProgressMutable.onNext(
                    MediaPlayerProgress(
                        mediaPlayer!!.currentPosition,
                        mediaPlayer!!.duration
                    )
                )
            }
        )
    }

    private fun resetAudio() {
        releaseMediaPlayer()
        audioRecorder!!.removeLastOutput()
        viewStateMutable.onNext(AudioViewState.Initial)
        _events.onNext(RecordAudioEvent.Vibrate)
    }

    private fun pauseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer!!.pause()
        viewStateMutable.onNext(
            AudioViewState.Paused(
                mediaPlayer!!.duration,
                mediaPlayer!!.currentPosition
            )
        )
    }

    private fun initMediaPlayer(audioFile: File) {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        mediaPlayer!!.setOnCompletionListener {
            mediaPlayerPositionJob?.cancel()
            initMediaPlayer(audioFile)
            viewStateMutable.onNext(AudioViewState.RecordingCompleted(mediaPlayer!!.duration))
            _events.onNext(RecordAudioEvent.Vibrate)
        }
    }

    private fun releaseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
    }

    private fun initAudioAacRecorder(context: Context) {
        val externalFilesDir = context.getExternalFilesDir(null)!!
        val audioRecordingsDirectory =
            File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}")
        if (!audioRecordingsDirectory.isDirectory) {
            audioRecordingsDirectory.mkdir()
        }
        audioRecorder = AudioRecorder.Builder(context)
            .setAudioSource(MediaRecorder.AudioSource.MIC)
            .setOutputFormat(MediaRecorder.OutputFormat.AAC_ADTS)
            .setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
            .saveRecordingsToDirectory(audioRecordingsDirectory)
            .build()
        audioRecorder!!.setOnRecordingTimeChangesListener { milliseconds ->
            recordingTimeMutable.onNext(milliseconds)
        }
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
        audioRecorder?.let {
            if (it.isRecording) {
                it.stopRecording()
            }
            if (shouldDeleteAudioInExit) {
                it.removeLastOutput()
            }
        }
        disposable.dispose()
    }
}