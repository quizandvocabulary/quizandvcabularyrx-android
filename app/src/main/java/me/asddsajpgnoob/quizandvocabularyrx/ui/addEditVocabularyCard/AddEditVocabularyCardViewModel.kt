package me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard

import android.media.MediaPlayer
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.data.local.preferences.InterfaceSettingsPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabularyrx.data.modifiers.VocabularyCardModifiers.domainToEntity
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.InterfaceSettingsRepository
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.VocabularyCardRepository
import me.asddsajpgnoob.quizandvocabularyrx.eventBus.EventBus
import me.asddsajpgnoob.quizandvocabularyrx.eventBus.model.VocabularyCardEvent
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model.AddEditVocabularyEvent
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model.AudioListItem
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model.ExampleListItem
import me.asddsajpgnoob.quizandvocabularyrx.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabularyrx.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabularyrx.util.Constants
import me.asddsajpgnoob.quizandvocabularyrx.util.KOptional
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventHandler
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventReceiver
import me.asddsajpgnoob.quizandvocabularyrx.util.kOptional
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

@ExperimentalCoroutinesApi
class AddEditVocabularyCardViewModel(
    savedStateHandle: SavedStateHandle,
    interfaceSettingsRepository: InterfaceSettingsRepository,
    private val vocabularyCardRepository: VocabularyCardRepository,
    private val externalFilesDir: File
) : ViewModel() {

    companion object {
        private const val KEY_CARD = "card"
    }

    private val disposable = CompositeDisposable()

    val initialVocabularyCard: VocabularyCard? = savedStateHandle.get(KEY_CARD)

    private var finalVocabularyCard = initialVocabularyCard

    private val _textOrigin: BehaviorSubject<KOptional<String>> =
        BehaviorSubject.createDefault(
            KOptional.wrap {
                initialVocabularyCard?.origin
            }
        )

    private val _textTranslation: BehaviorSubject<KOptional<String>> =
        BehaviorSubject.createDefault(
            KOptional.wrap {
                initialVocabularyCard?.translation
            }
        )

    private val openTranslatorOnTopOfApp: Flowable<Boolean> =
        interfaceSettingsRepository.openTranslatorOnTopOfApp

    private val _showInQuiz: BehaviorSubject<Boolean> =
        BehaviorSubject.createDefault(
            initialVocabularyCard?.showInQuiz ?: Constants.ADD_EDIT_CARD_SHOW_IN_QUIZ_INITIAL_VALUE
        )
    val showInQuiz: Observable<Boolean> get() = _showInQuiz

    private val _examples: BehaviorSubject<List<String>> = BehaviorSubject.createDefault(
        mutableListOf<String>().apply {
            initialVocabularyCard?.let {
                addAll(it.examples)
            }
        }
    )

    val examplesListItems: Observable<List<ExampleListItem>> = _examples.map { examples ->
        val result = mutableListOf<ExampleListItem>()
        for (i in examples.indices) {
            result.add(ExampleListItem.Example(examples[i]))
        }
        result.add(ExampleListItem.Footer)
        result
    }

    private val _audioFiles: BehaviorSubject<List<String>> = BehaviorSubject.createDefault(
        mutableListOf<String>().apply {
            initialVocabularyCard?.let {
                addAll(it.audioFiles)
            }
        }
    )

    val audioListItems: Observable<List<AudioListItem>> = _audioFiles.map { audioFiles ->
        val result = mutableListOf<AudioListItem>()
        for (i in audioFiles.indices) {
            result.add(AudioListItem.Audio(audioFiles[i]))
        }
        result.add(AudioListItem.Footer)
        result
    }

    val submitButtonEnabled: Observable<Boolean> = Observable.combineLatest(
        _textOrigin,
        _textTranslation,
        _showInQuiz,
        _examples,
        _audioFiles
    ) { textOrigin: KOptional<String>, textTranslation: KOptional<String>, showInQuizValue: Boolean, examplesList: List<String>, audioFilesList: List<String> ->
        finalVocabularyCard = VocabularyCard(
            textOrigin.getOr {
                ""
            },
            textTranslation.getOr {
                ""
            },
            examplesList.filter {
                it.isNotBlank()
            },
            audioFilesList,
            showInQuizValue,
            initialVocabularyCard?.guessedCount ?: 0,
            initialVocabularyCard?.notGuessedCount ?: 0,
            initialVocabularyCard?.createdAt ?: Date(),
            Date(),
            initialVocabularyCard?.id ?: 0L
        )
        finalVocabularyCard!!.run {
            origin.isNotBlank() &&
                    translation.isNotBlank() &&
                    (origin != initialVocabularyCard?.origin ?: "" ||
                            translation != initialVocabularyCard?.translation ?: "" ||
                            showInQuiz != initialVocabularyCard?.showInQuiz ?: Constants.ADD_EDIT_CARD_SHOW_IN_QUIZ_INITIAL_VALUE ||
                            examples != initialVocabularyCard?.examples ?: listOf<String>() ||
                            audioFiles != initialVocabularyCard?.audioFiles ?: listOf<String>()
                            )

        }
    }.subscribeOn(Schedulers.computation())

    private val _playbackInfo: BehaviorSubject<KOptional<PlaybackInfo>> =
        BehaviorSubject.createDefault(KOptional.empty())
    val playbackInfo: Observable<KOptional<PlaybackInfo>> get() = _playbackInfo

    private val _events = EventHandler<AddEditVocabularyEvent>()
    val events: EventReceiver<AddEditVocabularyEvent> get() = _events

    private var mediaPlayer: MediaPlayer? = null

    private val removedAudioFileNames = mutableListOf<String>()

    private var cardDeleted = false
    private var submitted = false

    fun getCurrentOrigin(): KOptional<String> {
        return _textOrigin.value!!
    }

    fun getCurrentTranslation(): KOptional<String> {
        return _textTranslation.value!!
    }

    fun getCurrentPlaybackInfo(): KOptional<PlaybackInfo> {
        return _playbackInfo.value!!
    }

    fun onDeleteExampleClicked(example: ExampleListItem.Example) {
        disposable.add(
            Single.create<List<String>> {
                val result = _examples.value!!.toMutableList()
                result.remove(example.example)
                it.onSuccess(result)
            }.subscribe { examples ->
                _examples.onNext(examples)
            }
        )
    }

    fun onExamplesEdited(position: Int, newValue: String) {
        disposable.add(
            Single.create<List<String>> {
                val result = _examples.value!!.toMutableList()
                result[position] = newValue
                it.onSuccess(result)
            }.subscribe { examples ->
                _examples.onNext(examples)
            }
        )
    }

    fun onDeleteAudioClicked(audio: AudioListItem.Audio) {
        val playingInfo: KOptional<PlaybackInfo> = _playbackInfo.value!!
        if (playingInfo.value != null && playingInfo.value.playing && playingInfo.value.fileName == audio.fileName) {
            releaseMediaPlayer()
        }
        removedAudioFileNames.add(audio.fileName)
        val result = _audioFiles.value!!.toMutableList()
        result.remove(audio.fileName)
        _audioFiles.onNext(result)
    }

    fun onEditTextOriginTextChanges(origin: String?) {
        _textOrigin.onNext(origin.kOptional())
    }

    fun onEditTextTranslationChanges(translation: String?) {
        _textTranslation.onNext(translation.kOptional())
    }

    fun onCheckboxShowInQuizCheckedChange(checked: Boolean) {
        _showInQuiz.onNext(checked)
    }

    fun onButtonSubmitClicked() {
        submitted = true
        if (initialVocabularyCard == null) {
            disposable.add(
                vocabularyCardRepository.insert(finalVocabularyCard!!.domainToEntity())
                    .subscribe { id ->
                        finalVocabularyCard = finalVocabularyCard!!.copy(
                            id = id
                        )
                        EventBus.submitVocabularyEvent(
                            VocabularyCardEvent.CardCreated(
                                finalVocabularyCard!!
                            )
                        )
                        _events.onNext(AddEditVocabularyEvent.PopBackStack)
                    }
            )
        } else {
            disposable.add(
                vocabularyCardRepository.update(finalVocabularyCard!!.domainToEntity()).subscribe {
                    EventBus.submitVocabularyEvent(
                        VocabularyCardEvent.CardEdited(
                            initialVocabularyCard,
                            finalVocabularyCard!!
                        )
                    )
                    _events.onNext(AddEditVocabularyEvent.PopBackStack)
                }
            )
        }
    }

    fun onImageButtonSearchOriginClicked() {
        _textOrigin.value!!.value.let {
            if (!it.isNullOrBlank()) {
                disposable.add(
                    openTranslatorOnTopOfApp.first(InterfaceSettingsPreferences.DEFAULT_OPEN_TRANSLATOR_ON_TOP_OF_APP)
                        .subscribe { openOnTopOfApp ->
                            _events.onNext(
                                AddEditVocabularyEvent.NavigateToGoogleTranslate(
                                    it,
                                    openOnTopOfApp
                                )
                            )
                        }
                )
            }
        }
    }

    fun onMenuItemDeleteCardSelected() {
        disposable.add(
            vocabularyCardRepository.delete(initialVocabularyCard!!.domainToEntity()).subscribe {
                cardDeleted = true
                EventBus.submitVocabularyEvent(VocabularyCardEvent.CardDeleted(initialVocabularyCard))
                _events.onNext(AddEditVocabularyEvent.PopBackStack)
            }
        )
    }

    fun onAddAudioClicked() {
        _events.onNext(AddEditVocabularyEvent.NavigateToRecordAudioDialog)
    }

    fun onAudioRecorded(audioFile: File) {
        val result = _audioFiles.value!!.toMutableList()
        result.add(audioFile.name)
        _audioFiles.onNext(result)
    }

    fun onAddExampleClicked() {
        val result = _examples.value!!.toMutableList()
        result.add("")
        _examples.onNext(result)
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    fun onPlayPauseClicked(item: AudioListItem.Audio) {
        val playingInfo = _playbackInfo.value!!
        if (playingInfo.value != null && playingInfo.value.fileName != item.fileName) {
            releaseMediaPlayer()
        }
        if (!isMediaPlayerPlaying()) {
            if (mediaPlayer == null) {
                val audioFile =
                    File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}/${item.fileName}")
                if (!audioFile.exists()) {
                    showToast(R.string.Audio_file_not_found)
                    return
                }
                initMediaPlayer(audioFile)
            }
            startMediaPlayer()
            return
        }
        pauseMediaPlayer()
    }

    private fun showToast(@StringRes stringRes: Int) {
        _events.onNext(AddEditVocabularyEvent.ShowToast(stringRes))
    }

    private fun initMediaPlayer(audioFile: File) {
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        _playbackInfo.onNext(
            PlaybackInfo(
                false,
                audioFile.name,
                mediaPlayer!!.duration,
                0
            ).kOptional()
        )
        mediaPlayer!!.setOnCompletionListener {
            releaseMediaPlayer()
            _playbackInfo.onNext(KOptional.empty())
        }
    }

    private fun startMediaPlayer() {
        val interval = Observable.interval(0L, 20L, TimeUnit.MILLISECONDS)
            .takeWhile {
                isMediaPlayerPlaying()
            }
        disposable.add(
            interval.subscribe {
                val playback = _playbackInfo.value!!
                if (playback.value != null) {
                    _playbackInfo.onNext(
                        playback.value.copy(
                            playing = true,
                            playingPosition = mediaPlayer!!.currentPosition
                        ).kOptional()
                    )
                }
            }
        )
        mediaPlayer!!.start()
    }

    private fun pauseMediaPlayer() {
        mediaPlayer!!.pause()
        val playback = _playbackInfo.value!!
        if (playback.value != null) {
            _playbackInfo.onNext(
                playback.value.copy(
                    playing = false
                ).kOptional()
            )
        }
    }

    private fun releaseMediaPlayer() {
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
        _playbackInfo.onNext(KOptional.empty())
    }

    private fun deleteRemovedAudioFiles() {
        val audiosDirectoryPath =
            "${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}"
        removedAudioFileNames.forEach { fileName ->
            val file = File("${audiosDirectoryPath}/$fileName")
            file.delete()
        }
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()

        val audiosDirectoryPath =
            "${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}"

        if (cardDeleted) {
            deleteRemovedAudioFiles()
            if (finalVocabularyCard != null) {
                finalVocabularyCard!!.audioFiles.forEach { fileName ->
                    val audioFile = File("${audiosDirectoryPath}/$fileName")
                    audioFile.delete()
                }
            } else {
                initialVocabularyCard!!.audioFiles.forEach { fileName ->
                    val audioFile = File("${audiosDirectoryPath}/$fileName")
                    audioFile.delete()
                }
            }
            return
        }

        if (submitted) {
            deleteRemovedAudioFiles()
        } else {
            finalVocabularyCard?.let { finalCard ->
                if (initialVocabularyCard == null) {
                    deleteRemovedAudioFiles()
                    finalCard.audioFiles.forEach { fileName ->
                        val audioFile = File("${audiosDirectoryPath}/$fileName")
                        audioFile.delete()
                    }
                } else {
                    removedAudioFileNames.forEach { fileName ->
                        if (initialVocabularyCard.audioFiles.contains(fileName)) {
                            return@forEach
                        }
                        val audioFile = File("${audiosDirectoryPath}/$fileName")
                        audioFile.delete()
                    }
                    finalCard.audioFiles.forEach { fileName ->
                        if (initialVocabularyCard.audioFiles.contains(fileName)) {
                            return@forEach
                        }
                        val audioFile = File("${audiosDirectoryPath}/$fileName")
                        audioFile.delete()
                    }
                }
            }
        }
        disposable.dispose()
    }

    class Factory(
        owner: SavedStateRegistryOwner,
        navArgsBundle: Bundle?,
        private val interfaceSettingsRepository: InterfaceSettingsRepository,
        private val vocabularyCardRepository: VocabularyCardRepository,
        private val externalFilesDir: File
    ) : AbstractSavedStateViewModelFactory(owner, navArgsBundle) {

        override fun <T : ViewModel> create(
            key: String,
            modelClass: Class<T>,
            handle: SavedStateHandle
        ): T {
            return if (modelClass.isAssignableFrom(AddEditVocabularyCardViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                AddEditVocabularyCardViewModel(
                    handle,
                    interfaceSettingsRepository,
                    vocabularyCardRepository,
                    externalFilesDir
                ) as T
            } else {
                throw IllegalArgumentException("Unable to create ${AddEditVocabularyCardViewModel::class.qualifiedName}")
            }
        }
    }

}