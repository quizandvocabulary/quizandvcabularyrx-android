package me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl

import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.data.local.preferences.QuizSettingsPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.QuizSettingsRepository

@ExperimentalCoroutinesApi
object QuizSettingsRepositoryImpl : QuizSettingsRepository{

    override val quizMode = QuizSettingsPreferences.quizMode

    override fun setQuizMode(quizMode: QuizModePreferences) {
        QuizSettingsPreferences.setQuizMode(quizMode)
    }
}