package me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl

import me.asddsajpgnoob.quizandvocabularyrx.data.local.VocabularyCardsDatabase
import me.asddsajpgnoob.quizandvocabularyrx.data.model.room.VocabularyCardEntity
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.VocabularyCardRepository

object VocabularyCardRepositoryImpl : VocabularyCardRepository {

    private val db = VocabularyCardsDatabase.get()

    private val dao = db.vocabularyCardDao()

    override fun searchVocabularyCardsByOriginOrTranslationOrderByCreatedAtDesc(
        query: String,
        offset: Int,
        limit: Int
    ) = dao.searchByOriginOrTranslationOrderByCreatedAtDesc(query, offset, limit)

    override fun findRandomVocabularyCard(exceptionIds: List<Long>) =
        dao.findRandomWithExceptions(exceptionIds)

    override fun findLessGuessedVocabularyCards(
        offset: Int,
        limit: Int
    ) = dao.findAllOrderByLessGuessedDesc(offset, limit)

    override fun findNewestVocabularyCards(offset: Int, limit: Int) = dao.findAllOrderByCreatedAtDesc(offset, limit)

    override fun findOldestVocabularyCards(offset: Int, limit: Int) = dao.findAllOrderByCreatedAtAsc(offset, limit)

    override fun insert(entity: VocabularyCardEntity) = dao.insert(entity)

    override fun update(entity: VocabularyCardEntity) = dao.update(entity)

    override fun delete(entity: VocabularyCardEntity) = dao.delete(entity)
}