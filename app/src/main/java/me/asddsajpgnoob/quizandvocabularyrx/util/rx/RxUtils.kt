package me.asddsajpgnoob.quizandvocabularyrx.util.rx

import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.emptyPreferences
import io.reactivex.rxjava3.core.Flowable
import java.io.IOException

fun Flowable<Preferences>.emitEmptyIfIOException(): Flowable<Preferences> {
    return onErrorReturn { e ->
        if (e is IOException) {
            emptyPreferences()
        } else {
            throw e
        }
    }
}