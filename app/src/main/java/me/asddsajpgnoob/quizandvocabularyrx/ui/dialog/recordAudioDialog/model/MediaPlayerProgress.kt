package me.asddsajpgnoob.quizandvocabularyrx.ui.dialog.recordAudioDialog.model

data class MediaPlayerProgress(
    val progress: Int,
    val duration: Int
)