package me.asddsajpgnoob.quizandvocabularyrx.data.local

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import me.asddsajpgnoob.quizandvocabularyrx.App
import me.asddsajpgnoob.quizandvocabularyrx.BuildConfig
import me.asddsajpgnoob.quizandvocabularyrx.data.local.dao.VocabularyCardDao
import me.asddsajpgnoob.quizandvocabularyrx.data.model.room.VocabularyCardEntity

@Database(
    version = 1,
    entities = [
        VocabularyCardEntity::class
    ]
)
abstract class VocabularyCardsDatabase : RoomDatabase() {

    companion object {
        private var INSTANCE: VocabularyCardsDatabase? = null

        fun get() = INSTANCE ?: synchronized(this) {
            Room.databaseBuilder(App.INSTANCE, VocabularyCardsDatabase::class.java, BuildConfig.DATABASE_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
                .also {
                    INSTANCE = it
                }
        }
    }

    abstract fun vocabularyCardDao(): VocabularyCardDao
}