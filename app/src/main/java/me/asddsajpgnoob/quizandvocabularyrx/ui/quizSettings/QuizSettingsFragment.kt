package me.asddsajpgnoob.quizandvocabularyrx.ui.quizSettings

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.QuizModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.QuizSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.databinding.FragmentQuizSettingsBinding
import me.asddsajpgnoob.quizandvocabularyrx.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabularyrx.ui.main.MainViewModel

@ExperimentalCoroutinesApi
class QuizSettingsFragment : BaseFragment<FragmentQuizSettingsBinding>() {

    private val viewModel: QuizSettingsViewModel by viewModels {
        QuizSettingsViewModel.Factory(QuizSettingsRepositoryImpl)
    }
    
    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentQuizSettingsBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val root = super.onCreateView(inflater, container, savedInstanceState)

        setOnClickListeners()

        collectQuizMode()

        return root
    }

    private fun setOnClickListeners() {
        binding.apply {
            radioButtonRandom.setOnClickListener {
                viewModel.onRadioButtonRandomClicked()
            }
            radioButtonLessGuessedFirst.setOnClickListener {
                viewModel.onRadioButtonLessGuessedFirstClicked()
            }
            radioButtonLessGuessedFirstRandom.setOnClickListener {
                viewModel.onRadioButtonLessGuessedFirstRandomClicked()
            }
            radioButtonNewestFirst.setOnClickListener {
                viewModel.onRadioButtonNewestFirstClicked()
            }
            radioButtonOldestFirst.setOnClickListener {
                viewModel.onRadioButtonOldestFirst()
            }
        }
    }

    @Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
    private fun collectQuizMode() {
        viewDisposable.add(
            viewModel.quizMode
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.apply {
                        when(it) {
                            QuizModePreferences.RANDOM -> {
                                radioButtonRandom.isChecked = true
                            }
                            QuizModePreferences.FIRST_LESS_GUESSED -> {
                                radioButtonLessGuessedFirst.isChecked = true
                            }
                            QuizModePreferences.FIRST_LESS_GUESSED_WITH_RANDOM -> {
                                radioButtonLessGuessedFirstRandom.isChecked = true
                            }
                            QuizModePreferences.NEWEST_FIRST -> {
                                radioButtonNewestFirst.isChecked = true
                            }
                            QuizModePreferences.OLDEST_FIRST -> {
                                radioButtonOldestFirst.isChecked = true
                            }
                        }
                    }
                }
        )
    }
}