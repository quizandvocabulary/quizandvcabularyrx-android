package me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.model

import androidx.annotation.StringRes
import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard

sealed interface VocabularyEvent {

    object NavigateToAddVocabularyCardFragment : VocabularyEvent

    data class NavigateToEditVocabularyCardFragment(val card: VocabularyCard) : VocabularyEvent

    data class ShowToast(@StringRes val stringRes: Int) : VocabularyEvent
}