package me.asddsajpgnoob.quizandvocabularyrx.data.local.preferences

import android.content.Context
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.rxjava3.rxPreferencesDataStore
import androidx.datastore.rxjava3.RxDataStore
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.App
import me.asddsajpgnoob.quizandvocabularyrx.util.rx.emitEmptyIfIOException

@ExperimentalCoroutinesApi
object OnTimeActionPreferences {

    private object OnTimeActionKeys {
        val SHOW_QUIZ_TOUCH_HINT = booleanPreferencesKey("showQuizTouchHint")
        val SHOW_QUIZ_SWIPE_DIRECTION_HINT = booleanPreferencesKey("showQuizSwipeDirectionHint")
    }

    const val DEFAULT_SHOW_QUIZ_TOUCH_HINT = true
    const val DEFAULT_SHOW_QUIZ_SWIPE_DIRECTION_HINT = true

    private val Context.onTimeActionPreferences: RxDataStore<Preferences> by rxPreferencesDataStore(
        name = "on_time_action_preferences"
    )

    val showQuizTouchHint: Flowable<Boolean> = App.INSTANCE.onTimeActionPreferences.data()
        .emitEmptyIfIOException()
        .map { preferences ->
            preferences[OnTimeActionKeys.SHOW_QUIZ_TOUCH_HINT]
                ?: DEFAULT_SHOW_QUIZ_TOUCH_HINT
        }

    val showQuizSwipeDirectionHint: Flowable<Boolean> =
        App.INSTANCE.onTimeActionPreferences.data()
            .emitEmptyIfIOException()
            .map { preferences ->
                preferences[OnTimeActionKeys.SHOW_QUIZ_SWIPE_DIRECTION_HINT]
                    ?: DEFAULT_SHOW_QUIZ_SWIPE_DIRECTION_HINT
            }

    fun setShowQuizTouchHint(show: Boolean) {
        App.INSTANCE.onTimeActionPreferences.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[OnTimeActionKeys.SHOW_QUIZ_TOUCH_HINT] = show
                it.onSuccess(prefs)
            }
        }
    }

    fun setShowQuizSwipeDirectionHint(show: Boolean) {
        App.INSTANCE.onTimeActionPreferences.updateDataAsync { preferences ->
            Single.create {
                val prefs = preferences.toMutablePreferences()
                prefs[OnTimeActionKeys.SHOW_QUIZ_SWIPE_DIRECTION_HINT] = show
                it.onSuccess(prefs)
            }
        }
    }
}