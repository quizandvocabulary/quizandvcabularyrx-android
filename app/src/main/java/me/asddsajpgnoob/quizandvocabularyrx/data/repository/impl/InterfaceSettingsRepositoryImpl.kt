package me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl

import io.reactivex.rxjava3.core.Flowable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.data.local.preferences.InterfaceSettingsPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.InterfaceSettingsRepository

@ExperimentalCoroutinesApi
object InterfaceSettingsRepositoryImpl : InterfaceSettingsRepository {

    override val darkMode: Flowable<DarkModePreferences> = InterfaceSettingsPreferences.darkMode

    override val defaultTab: Flowable<DefaultTabPreferences> = InterfaceSettingsPreferences.defaultTab

    override val showAudiosInVocabularyScreen: Flowable<Boolean> = InterfaceSettingsPreferences.showAudiosInVocabularyScreen

    override val showExamplesInVocabularyScreen: Flowable<Boolean> = InterfaceSettingsPreferences.showExamplesInVocabularyScreen

    override val openTranslatorOnTopOfApp: Flowable<Boolean> = InterfaceSettingsPreferences.openTranslatorOnTopOfApp

    override val saveNavigationStateWhenSwitchingTab: Flowable<Boolean> = InterfaceSettingsPreferences.saveNavigationStateWhenSwitchingTab

    override fun setDarkMode(darkMode: DarkModePreferences) {
        InterfaceSettingsPreferences.setDarkMode(darkMode)
    }

    override fun setDefaultTab(defaultTab: DefaultTabPreferences) {
        InterfaceSettingsPreferences.setDefaultTab(defaultTab)
    }

    override fun setShowAudiosInVocabularyScreen(show: Boolean) {
        InterfaceSettingsPreferences.setShowAudiosInVocabularyScreen(show)
    }

    override fun setExamplesAudiosInVocabularyScreen(show: Boolean) {
        InterfaceSettingsPreferences.setExamplesAudiosInVocabularyScreen(show)
    }

    override fun setOpenTranslatorOnTopOfApp(open: Boolean) {
        InterfaceSettingsPreferences.setOpenTranslatorOnTopOfApp(open)
    }

    override fun setSaveNavigationStateWhenSwitchingTab(save: Boolean) {
        InterfaceSettingsPreferences.setSaveNavigationStateWhenSwitchingTab(save)
    }

}

