package me.asddsajpgnoob.quizandvocabularyrx.ui.main

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.databinding.ActivityMainBinding
import me.asddsajpgnoob.quizandvocabularyrx.ui.base.BaseActivity
import me.asddsajpgnoob.quizandvocabularyrx.util.DisplayMeasureUtils
import me.asddsajpgnoob.quizandvocabularyrx.util.UiStateUtils
import me.asddsajpgnoob.quizandvocabularyrx.util.UiStateUtils.makeStatusBarTransparent
import me.asddsajpgnoob.quizandvocabularyrx.util.UiStateUtils.setLightStatusBar
import me.asddsajpgnoob.quizandvocabularyrx.util.customSetupWithNavController

@ExperimentalCoroutinesApi
class MainActivity : BaseActivity<ActivityMainBinding>() {

    companion object {
        var screenOrientation = Configuration.ORIENTATION_PORTRAIT
            private set
    }

    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    private val viewModel: MainViewModel by viewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = ActivityMainBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView()

        screenOrientation = resources.configuration.orientation

        setSupportActionBar(binding.toolbar)

        collectDarkMode()
        collectScreenOrientation()

        makeStatusBarTransparent()

        val statusBarParams =
            binding.statusBarPlaceholder.layoutParams as ConstraintLayout.LayoutParams
        statusBarParams.height = DisplayMeasureUtils.getStatusBarHeight(resources)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        navController = navHostFragment.findNavController()

        var graph: NavGraph? = viewModel.graph
        if (graph == null) {
            val startDestinationId = when (viewModel.defaultTab) {
                DefaultTabPreferences.QUIZ -> {
                    R.id.navigation_quiz
                }
                DefaultTabPreferences.VOCABULARY -> {
                    R.id.navigation_vocabulary
                }
            }
            graph = navController.navInflater.inflate(R.navigation.mobile_navigation)
            graph.setStartDestination(startDestinationId)
        }

        navController.graph = graph
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_quiz,
                R.id.navigation_vocabulary
            )
        )

        setupActionBarWithNavController(navController, appBarConfiguration)
        binding.navView.customSetupWithNavController(navController) {
            viewModel.currentSaveNavigationState
        }
    }

    @Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
    private fun collectDarkMode() {
        disposable.add(
            viewModel.darkMode
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        DarkModePreferences.DARK -> {
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                            setLightStatusBar(false)
                        }
                        DarkModePreferences.LIGHT -> {
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                            setLightStatusBar(true)
                        }
                        DarkModePreferences.FOLLOW_SYSTEM -> {
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                            setLightStatusBar(!UiStateUtils.isNightMode(resources))
                        }
                    }
                }
        )
    }

    private fun collectScreenOrientation() {
        disposable.add(
            viewModel.orientation
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    requestedOrientation = it
                }

        )
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.saveGraph(navController.graph)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}