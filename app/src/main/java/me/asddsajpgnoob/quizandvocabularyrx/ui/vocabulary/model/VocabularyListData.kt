package me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.model

import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard

data class VocabularyListData(
    val items: List<VocabularyCard>,
    val showAudios: Boolean,
    val showExamples: Boolean,
    val highlight: String?
)