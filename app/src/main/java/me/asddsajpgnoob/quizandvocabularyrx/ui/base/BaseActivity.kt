package me.asddsajpgnoob.quizandvocabularyrx.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    private var _disposable: CompositeDisposable? = null
    protected val disposable: CompositeDisposable get() = _disposable!!

    abstract fun getViewBinding(): VB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _disposable = CompositeDisposable()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
        _disposable = null
    }


    protected fun bindView() {
        _binding = getViewBinding()
        setContentView(binding.root)
    }

    protected fun showToast(toast: String) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show()
    }

    protected fun showToast(@StringRes stringRes: Int) {
        Toast.makeText(this, stringRes, Toast.LENGTH_SHORT).show()
    }
}