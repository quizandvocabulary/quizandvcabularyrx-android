package me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary

import android.media.MediaPlayer
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.processors.PublishProcessor
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import kotlinx.coroutines.Job
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabularyrx.data.model.room.VocabularyCardEntity
import me.asddsajpgnoob.quizandvocabularyrx.data.modifiers.VocabularyCardModifiers.domainToEntity
import me.asddsajpgnoob.quizandvocabularyrx.data.modifiers.VocabularyCardModifiers.entityListToDomainList
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.InterfaceSettingsRepository
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.VocabularyCardRepository
import me.asddsajpgnoob.quizandvocabularyrx.eventBus.EventBus
import me.asddsajpgnoob.quizandvocabularyrx.eventBus.model.VocabularyCardEvent
import me.asddsajpgnoob.quizandvocabularyrx.ui.common.model.PlaybackInfo
import me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.model.CardAndPlaybackInfo
import me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.model.VocabularyEvent
import me.asddsajpgnoob.quizandvocabularyrx.ui.vocabulary.model.VocabularyListData
import me.asddsajpgnoob.quizandvocabularyrx.util.AudioPlayerFactory
import me.asddsajpgnoob.quizandvocabularyrx.util.Constants
import me.asddsajpgnoob.quizandvocabularyrx.util.KOptional
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventHandler
import me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler.EventReceiver
import me.asddsajpgnoob.quizandvocabularyrx.util.kOptional
import java.io.File
import java.util.concurrent.TimeUnit

class VocabularyViewModel(
    interfaceSettingsRepository: InterfaceSettingsRepository,
    private val vocabularyCardRepository: VocabularyCardRepository,
    private val externalFilesDir: File
) : ViewModel() {

    companion object {
        private const val LIMIT = 10
    }

    private val disposable = CompositeDisposable()

    private val _searchQuery: BehaviorSubject<KOptional<String>> =
        BehaviorSubject.createDefault(KOptional.empty())

    private val _searchedVocabularyCards: BehaviorSubject<List<VocabularyCard>> =
        BehaviorSubject.createDefault(listOf())

    private val _staticVocabularyCards: BehaviorSubject<List<VocabularyCard>> =
        BehaviorSubject.createDefault(
            listOf()
        )

    private val showAudiosFlow = interfaceSettingsRepository.showAudiosInVocabularyScreen

    private val showExamplesFlow = interfaceSettingsRepository.showExamplesInVocabularyScreen

    val listItems: Observable<VocabularyListData> =
        Observable.combineLatest(
            _searchQuery,
            _searchedVocabularyCards,
            _staticVocabularyCards,
            showAudiosFlow.toObservable(),
            showExamplesFlow.toObservable()
        ) { query, searchedCards, staticCards, showAudios, showExamples ->
            val items: List<VocabularyCard>
            val highlight: String?
            if (query.value.isNullOrBlank()) {
                items = staticCards
                highlight = null
            } else {
                items = searchedCards
                highlight = query.value
            }
            VocabularyListData(
                items,
                showAudios,
                showExamples,
                highlight
            )
        }.subscribeOn(Schedulers.computation())

    private val _cardAndPlaybackInfo: BehaviorSubject<KOptional<CardAndPlaybackInfo>> =
        BehaviorSubject.createDefault(
            KOptional.empty()
        )
    val cardAndPlaybackInfo: Observable<KOptional<CardAndPlaybackInfo>> = _cardAndPlaybackInfo

    private val _events = EventHandler<VocabularyEvent>()
    val events: EventReceiver<VocabularyEvent> get() = _events

    private var mediaPlayerPositionJob: Job? = null
    private var mediaPlayer: MediaPlayer? = null

    private var staticCardsOffset = 0
    private var searchedCardsOffset = 0

    private var loadingStaticCards = false
    private var staticCardsEndOfPaginationReached = false

    private var loadingSearchedCards = false
    private var searchedCardsEndOfPaginationReached = false

    private var loadingSearchedCardsDisposable: Disposable? = null

    init {
        loadLocalVocabularyCards()
        disposable.add(
            EventBus.vocabularyCardEventBus.subscribe { event ->
                when(event) {
                    is VocabularyCardEvent.CardCreated -> {
                        onCardCreated(event.card)
                    }
                    is VocabularyCardEvent.CardDeleted -> {
                        onCardDeleted(event.card)
                    }
                    is VocabularyCardEvent.CardEdited -> {
                        onCardEdited(event.editedCard)
                    }
                }
            }
        )
    }

    fun getCurrentSearchQuery(): KOptional<String> {
        return _searchQuery.value!!
    }

    fun onListBottomReached() {
        if (_searchQuery.value!!.value.isNullOrBlank()) {
            if (!loadingStaticCards &&
                !staticCardsEndOfPaginationReached
            ) {
                loadLocalVocabularyCards()
            }
            return
        }
        if (!loadingSearchedCards &&
            !searchedCardsEndOfPaginationReached
        ) {
            loadLocalSearchedVocabularyCards(false)
        }
    }

    fun onItemSwiped(swipedCard: VocabularyCard) {
        deleteCardFromDb(swipedCard)
        val query = _searchQuery.value!!.value
        if (!query.isNullOrBlank()) {
            val searchedResult = _searchedVocabularyCards.value!!.toMutableList()
            val searchedIndex = searchedResult.indexOfFirst {
                it.id == swipedCard.id
            }
            if (searchedIndex != -1) {
                searchedResult.removeAt(searchedIndex)
                _searchedVocabularyCards.onNext(searchedResult)
                searchedCardsOffset--
            }
        }

        val staticResult = _staticVocabularyCards.value!!.toMutableList()
        val index = staticResult.indexOfFirst {
            it.id == swipedCard.id
        }
        if (index != -1) {
            staticResult.removeAt(index)
            _staticVocabularyCards.onNext(staticResult)
            staticCardsOffset--
        }
    }

    fun onSearchQueryChanged(query: String?) {
        if (_searchQuery.value!!.value == query) {
            return
        }
        _searchQuery.onNext(query.kOptional())
        loadingSearchedCardsDisposable?.dispose()

        if (query.isNullOrBlank()) {
            return
        }
        searchedCardsEndOfPaginationReached = false
        searchedCardsOffset = 0
        loadLocalSearchedVocabularyCards(true)
    }

    fun onFloatingButtonAddClicked() {
        _events.onNext(VocabularyEvent.NavigateToAddVocabularyCardFragment)
    }

    fun onCardClicked(card: VocabularyCard) {
        _events.onNext(VocabularyEvent.NavigateToEditVocabularyCardFragment(card))
    }

    fun onPlayPauseClicked(card: VocabularyCard, fileName: String) {
        val playbackInfo = _cardAndPlaybackInfo.value!!.value
        if (playbackInfo != null && playbackInfo.playbackInfo.fileName != fileName) {
            releaseMediaPlayer()
        }
        if (!isMediaPlayerPlaying()) {
            if (mediaPlayer == null) {
                val audioFile =
                    File("${externalFilesDir.absolutePath}/${Constants.AUDIOS_DIRECTORY_NAME}/${fileName}")
                if (!audioFile.exists()) {
                    showToast(R.string.Audio_file_not_found)
                    return
                }
                initMediaPlayer(
                    card,
                    audioFile
                )
            }
            startMediaPlayer()
            return
        }
        pauseMediaPlayer()
    }

    private fun showToast(@StringRes stringRes: Int) {
        _events.onNext(VocabularyEvent.ShowToast(stringRes))
    }

    private fun deleteCardFromDb(card: VocabularyCard) {
        vocabularyCardRepository.delete(card.domainToEntity()).subscribe {
            EventBus.submitVocabularyEvent(VocabularyCardEvent.CardDeleted(card))
        }
    }

    private fun loadLocalVocabularyCards() {
        loadingStaticCards = true
        val entitiesSingle = vocabularyCardRepository.findNewestVocabularyCards(
            staticCardsOffset,
            LIMIT
        )
        disposable.add(
            entitiesSingle.subscribe { entities: List<VocabularyCardEntity> ->
                if (entities.isEmpty()) {
                    staticCardsEndOfPaginationReached = true
                    return@subscribe
                }
                val result = _staticVocabularyCards.value!!.toMutableList()
                result.addAll(
                    entities.entityListToDomainList()
                )
                _staticVocabularyCards.onNext(result)
                staticCardsOffset += entities.size
                loadingStaticCards = false
            }
        )
    }

    private fun loadLocalSearchedVocabularyCards(isRefresh: Boolean) {
        loadingSearchedCards = true
        val entitiesSingle =
            vocabularyCardRepository.searchVocabularyCardsByOriginOrTranslationOrderByCreatedAtDesc(
                _searchQuery.value!!.value!!,
                searchedCardsOffset,
                LIMIT
            )
        loadingSearchedCardsDisposable = entitiesSingle.subscribe { entities ->
            if (entities.isEmpty()) {
                searchedCardsEndOfPaginationReached = true
            }
            val result = if (isRefresh) {
                mutableListOf()
            } else {
                _searchedVocabularyCards.value!!.toMutableList()
            }
            result.addAll(
                entities.entityListToDomainList()
            )
            _searchedVocabularyCards.onNext(result)
            searchedCardsOffset += entities.size
            loadingSearchedCards = false
        }
    }

    private fun isMediaPlayerPlaying() = mediaPlayer?.isPlaying == true

    private fun initMediaPlayer(card: VocabularyCard, audioFile: File) {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer = AudioPlayerFactory.newInstance(audioFile)
        mediaPlayer!!.prepare()
        _cardAndPlaybackInfo.onNext(
            CardAndPlaybackInfo(
                PlaybackInfo(
                    false,
                    audioFile.name,
                    mediaPlayer!!.duration,
                    0
                ),
                card
            ).kOptional()
        )
        mediaPlayer!!.setOnCompletionListener {
            releaseMediaPlayer()
            _cardAndPlaybackInfo.onNext(KOptional.empty())
        }
    }

    private fun startMediaPlayer() {
        mediaPlayer!!.start()
        val interval = Observable.interval(0L, 20L, TimeUnit.MILLISECONDS)
            .takeWhile {
                isMediaPlayerPlaying()
            }
        disposable.add(
            interval.subscribe {
                    _cardAndPlaybackInfo.onNext(
                        _cardAndPlaybackInfo.value!!.value!!.copy(
                            playbackInfo = _cardAndPlaybackInfo.value!!.value!!.playbackInfo.copy(
                                playing = true,
                                playingPosition = mediaPlayer!!.currentPosition
                            )
                        ).kOptional()
                    )
                }
        )
    }

    private fun pauseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        mediaPlayer!!.pause()
        _cardAndPlaybackInfo.onNext(
            _cardAndPlaybackInfo.value!!.value!!.copy(
                playbackInfo = _cardAndPlaybackInfo.value!!.value!!.playbackInfo.copy(
                    playing = false
                )
            ).kOptional()
        )
    }

    private fun releaseMediaPlayer() {
        mediaPlayerPositionJob?.cancel()
        if (isMediaPlayerPlaying()) {
            mediaPlayer!!.stop()
        }
        mediaPlayer?.release()
        mediaPlayer = null
        _cardAndPlaybackInfo.onNext(KOptional.empty())
    }

    private fun onCardCreated(addedCard: VocabularyCard) {
        val query = _searchQuery.value!!.value
        if (!query.isNullOrBlank() &&
            (addedCard.origin.contains(query, ignoreCase = true) || addedCard.translation.contains(
                query,
                ignoreCase = true
            ))
        ) {
            val searchedResult = _searchedVocabularyCards.value!!.toMutableList()
            searchedResult.add(0, addedCard)
            _searchedVocabularyCards.onNext(searchedResult)
            searchedCardsOffset++
        }

        val staticResult = _staticVocabularyCards.value!!.toMutableList()
        staticResult.add(0, addedCard)
        _staticVocabularyCards.onNext(staticResult)
        staticCardsOffset++
    }

    private fun onCardEdited(editedCard: VocabularyCard) {
        val query = _searchQuery.value!!.value
        if (!query.isNullOrBlank()) {
            val searchedResult = _searchedVocabularyCards.value!!.toMutableList()
            val searchedIndex = searchedResult.indexOfFirst {
                it.id == editedCard.id
            }
            if (searchedIndex != -1) {
                searchedResult[searchedIndex] = editedCard
                _searchedVocabularyCards.onNext(searchedResult)
            }
        }

        val staticResult = _staticVocabularyCards.value!!.toMutableList()
        val staticIndex = staticResult.indexOfFirst {
            it.id == editedCard.id
        }
        if (staticIndex != -1) {
            staticResult[staticIndex] = editedCard
            _staticVocabularyCards.onNext(staticResult)
        }
    }

    private fun onCardDeleted(deletedCard: VocabularyCard) {
        val query = _searchQuery.value!!.value
        if (!query.isNullOrBlank()) {
            val searchedResult = _searchedVocabularyCards.value!!.toMutableList()
            val searchedIndex = searchedResult.indexOfFirst {
                it.id == deletedCard.id
            }
            if (searchedIndex != -1) {
                searchedResult.removeAt(searchedIndex)
                _searchedVocabularyCards.onNext(searchedResult)
                searchedCardsOffset--
            }
        }

        val staticResult = _staticVocabularyCards.value!!.toMutableList()
        val staticIndex = staticResult.indexOfFirst {
            it.id == deletedCard.id
        }
        if (staticIndex != -1) {
            staticResult.removeAt(staticIndex)
            _staticVocabularyCards.onNext(staticResult)
            staticCardsOffset--
        }
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
        disposable.dispose()
        if (loadingSearchedCardsDisposable?.isDisposed == false) {
            loadingSearchedCardsDisposable?.dispose()
        }
    }

    class Factory(
        private val interfaceSettingsRepository: InterfaceSettingsRepository,
        private val vocabularyCardRepository: VocabularyCardRepository,
        private val externalFilesDir: File
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(VocabularyViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return VocabularyViewModel(
                    interfaceSettingsRepository,
                    vocabularyCardRepository,
                    externalFilesDir
                ) as T
            } else {
                throw IllegalArgumentException("Unable to create ${VocabularyViewModel::class.qualifiedName}")
            }
        }
    }
}