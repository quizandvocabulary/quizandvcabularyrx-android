package me.asddsajpgnoob.quizandvocabularyrx.ui.settings

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.BuildConfig
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.databinding.FragmentSettingsBinding
import me.asddsajpgnoob.quizandvocabularyrx.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabularyrx.ui.main.MainViewModel
import me.asddsajpgnoob.quizandvocabularyrx.ui.settings.model.SettingsEvent

@ExperimentalCoroutinesApi
class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {

    companion object {
        private const val CURRENT_DESTINATION_ID = R.id.settingsFragment
    }

    private val viewModel: SettingsViewModel by viewModels()

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentSettingsBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setupViews()
        setOnClickListeners()

        collectEvents()
        return view
    }

    private fun setupViews() {
        binding.apply {
            textAppVersion.text = BuildConfig.VERSION_NAME
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            textInterface.setOnClickListener {
                viewModel.onInterfaceLabelClick()
            }
            textQuiz.setOnClickListener {
                viewModel.onQuizClicked()
            }
        }
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onStart(owner: LifecycleOwner) {
                viewModel.events.subscribe {
                    Single.just(it)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { event ->
                            when (event) {
                                SettingsEvent.NavigateToInterfaceSettingsFragment -> {
                                    val action =
                                        SettingsFragmentDirections.actionSettingsFragmentToInterfaceSettingsFragment()
                                    navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                                }
                                SettingsEvent.NavigateToQuizSettingsFragment -> {
                                    val action =
                                        SettingsFragmentDirections.actionSettingsFragmentToQuizSettingsFragment()
                                    navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                                }
                            }
                        }
                }
            }

            override fun onStop(owner: LifecycleOwner) {
                viewModel.events.unsubscribe()
            }
        })
    }

}