package me.asddsajpgnoob.quizandvocabularyrx.data.repository

import io.reactivex.rxjava3.core.Flowable

interface OnTimeActionRepository {

    val showQuizTouchHint: Flowable<Boolean>

    val showQuizSwipeDirectionHint: Flowable<Boolean>

    fun setShowQuizTouchHint(show: Boolean)

    fun setShowQuizSwipeDirectionHint(show: Boolean)
}