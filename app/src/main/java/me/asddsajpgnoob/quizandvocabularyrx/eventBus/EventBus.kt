package me.asddsajpgnoob.quizandvocabularyrx.eventBus

import io.reactivex.rxjava3.processors.FlowableProcessor
import io.reactivex.rxjava3.processors.PublishProcessor
import me.asddsajpgnoob.quizandvocabularyrx.eventBus.model.VocabularyCardEvent

object EventBus {

    private val _vocabularyCardEventBus = PublishProcessor.create<VocabularyCardEvent>()
    val vocabularyCardEventBus: FlowableProcessor<VocabularyCardEvent> get() = _vocabularyCardEventBus

    fun submitVocabularyEvent(event: VocabularyCardEvent) {
        _vocabularyCardEventBus.onNext(event)
    }
}