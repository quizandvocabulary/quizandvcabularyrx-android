package me.asddsajpgnoob.quizandvocabularyrx.ui.main

import android.content.pm.ActivityInfo
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavGraph
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DarkModePreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.DefaultTabPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.InterfaceSettingsRepository

class MainViewModel(
    interfaceSettingsRepository: InterfaceSettingsRepository
) : ViewModel() {

    private val disposable = CompositeDisposable()

    val darkMode: Flowable<DarkModePreferences> = interfaceSettingsRepository.darkMode

    private val saveNavigationStateWhenSwitchingTab =
        interfaceSettingsRepository.saveNavigationStateWhenSwitchingTab

    val defaultTab: DefaultTabPreferences = interfaceSettingsRepository.defaultTab.blockingFirst()

    var currentSaveNavigationState: Boolean =
        interfaceSettingsRepository.saveNavigationStateWhenSwitchingTab.blockingFirst()
        private set

    var graph: NavGraph? = null
        private set

    init {
        disposable.add(
            saveNavigationStateWhenSwitchingTab
                .subscribe {
                    currentSaveNavigationState = it
                }
        )
    }

    private val _orientation: BehaviorSubject<Int> = BehaviorSubject.createDefault(
        when (defaultTab) {
            DefaultTabPreferences.QUIZ -> {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            }
            DefaultTabPreferences.VOCABULARY -> {
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            }
        }
    )
    val orientation: Observable<Int> get() = _orientation

    fun setOrientation(activityInfo: Int) {
        _orientation.onNext(activityInfo)
    }

    fun saveGraph(graph: NavGraph) {
        this.graph = graph
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    class Factory(private val interfaceSettingsRepository: InterfaceSettingsRepository) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                MainViewModel(interfaceSettingsRepository) as T
            } else {
                throw IllegalArgumentException("unable to create ${MainViewModel::class.qualifiedName}")
            }
        }
    }
}