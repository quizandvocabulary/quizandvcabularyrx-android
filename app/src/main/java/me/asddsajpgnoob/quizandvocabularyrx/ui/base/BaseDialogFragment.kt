package me.asddsajpgnoob.quizandvocabularyrx.ui.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import io.reactivex.rxjava3.disposables.CompositeDisposable
import me.asddsajpgnoob.quizandvocabularyrx.R

abstract class BaseDialogFragment<VB : ViewBinding> : DialogFragment() {

    private var _binding: VB? = null
    protected val binding get() = _binding!!

    private var _disposable: CompositeDisposable? = null
    protected val disposable: CompositeDisposable get() = _disposable!!

    private var _viewDisposable: CompositeDisposable? = null
    protected val viewDisposable: CompositeDisposable get() = _viewDisposable!!

    protected abstract fun getViewBinding(): VB

    abstract override fun isCancelable(): Boolean

    abstract fun getHeight(): Int

    abstract fun getWidth(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.MainDialogFragmentStyle)
        _disposable = CompositeDisposable()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = getViewBinding()
        _viewDisposable = CompositeDisposable()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window!!.setLayout(getWidth(), getHeight())
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.setCancelable(isCancelable)
        dialog.setCanceledOnTouchOutside(isCancelable)

        return dialog
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewDisposable.dispose()
        _viewDisposable = null
        _binding = null
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
        _disposable = null
    }

    protected fun showToast(stringRes: Int) {
        Toast.makeText(requireContext(), stringRes, Toast.LENGTH_SHORT).show()
    }

    protected fun showToast(string: String) {
        Toast.makeText(requireContext(), string, Toast.LENGTH_SHORT).show()
    }

    protected fun popBackStack() {
        findNavController().popBackStack()
    }

    protected fun popBackStackIfInDestination(@IdRes destinationId: Int): Boolean {
        val navController = findNavController()
        if (navController.currentDestination?.id == destinationId) {
            navController.popBackStack()
            return true
        }
        return false
    }
}