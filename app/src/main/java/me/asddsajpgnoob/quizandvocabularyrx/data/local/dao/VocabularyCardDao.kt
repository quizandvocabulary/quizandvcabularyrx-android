package me.asddsajpgnoob.quizandvocabularyrx.data.local.dao

import androidx.room.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import me.asddsajpgnoob.quizandvocabularyrx.data.model.room.VocabularyCardEntity

@Dao
interface VocabularyCardDao {

    @Query("SELECT * FROM vocabulary_cards ORDER BY datetime(created_at) DESC LIMIT :limit OFFSET :offset")
    fun findAllOrderByCreatedAtDesc(offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    @Query("SELECT * FROM vocabulary_cards ORDER BY datetime(created_at) ASC LIMIT :limit OFFSET :offset")
    fun findAllOrderByCreatedAtAsc(offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    @Query("SELECT * FROM vocabulary_cards WHERE origin LIKE '%' || :query || '%' OR translation LIKE '%' || :query || '%' ORDER BY datetime(created_at) DESC LIMIT :limit OFFSET :offset")
    fun searchByOriginOrTranslationOrderByCreatedAtDesc(query: String, offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    @Query("SELECT * FROM vocabulary_cards WHERE id NOT IN (:exceptionIds) AND show_in_quiz is 1 ORDER BY RANDOM() LIMIT 1")
    fun findRandomWithExceptions(exceptionIds: List<Long>): Maybe<VocabularyCardEntity>

    @Query("SELECT * FROM vocabulary_cards ORDER BY not_guessed_count DESC LIMIT :limit OFFSET :offset")
    fun findAllOrderByLessGuessedDesc(offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(card: VocabularyCardEntity): Single<Long>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(card: VocabularyCardEntity): Completable

    @Delete
    fun delete(card: VocabularyCardEntity): Completable
}