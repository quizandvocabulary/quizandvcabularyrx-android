package me.asddsajpgnoob.quizandvocabularyrx.ui.common.model

data class PlaybackInfo(
    val playing: Boolean,
    val fileName: String,
    val duration: Int,
    val playingPosition: Int
)
