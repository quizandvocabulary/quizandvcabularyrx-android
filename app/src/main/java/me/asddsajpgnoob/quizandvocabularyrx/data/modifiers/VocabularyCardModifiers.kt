package me.asddsajpgnoob.quizandvocabularyrx.data.modifiers

import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard
import me.asddsajpgnoob.quizandvocabularyrx.data.model.room.VocabularyCardEntity

object VocabularyCardModifiers {

    fun VocabularyCard.domainToEntity(): VocabularyCardEntity = VocabularyCardEntity(
        origin,
        translation,
        examples,
        audioFiles,
        showInQuiz,
        guessedCount,
        notGuessedCount,
        createdAt,
        updatedAt,
        id
    )

    fun VocabularyCardEntity.entityToDomain(): VocabularyCard = VocabularyCard(
        origin,
        translation,
        examples,
        audioFiles,
        showInQuiz,
        guessedCount,
        notGuessedCount,
        createdAt,
        updatedAt,
        id
    )

    fun List<VocabularyCardEntity>.entityListToDomainList(): List<VocabularyCard> = map {
        it.entityToDomain()
    }
}