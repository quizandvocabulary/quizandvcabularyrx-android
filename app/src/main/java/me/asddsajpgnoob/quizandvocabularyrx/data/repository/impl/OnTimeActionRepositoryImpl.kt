package me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl

import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.data.local.preferences.OnTimeActionPreferences
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.OnTimeActionRepository

@ExperimentalCoroutinesApi
object OnTimeActionRepositoryImpl : OnTimeActionRepository {

    override val showQuizTouchHint = OnTimeActionPreferences.showQuizTouchHint

    override val showQuizSwipeDirectionHint = OnTimeActionPreferences.showQuizSwipeDirectionHint

    override fun setShowQuizTouchHint(show: Boolean) {
        OnTimeActionPreferences.setShowQuizTouchHint(show)
    }

    override fun setShowQuizSwipeDirectionHint(show: Boolean) {
        OnTimeActionPreferences.setShowQuizSwipeDirectionHint(show)
    }
}