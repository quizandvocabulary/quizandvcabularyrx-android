package me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model

import androidx.annotation.StringRes
import me.asddsajpgnoob.quizandvocabularyrx.data.model.domain.VocabularyCard

sealed interface AddEditVocabularyEvent {

    object NavigateToRecordAudioDialog : AddEditVocabularyEvent

    data class NavigateToGoogleTranslate(val origin: String, val openOnTopOfApp: Boolean) : AddEditVocabularyEvent

    object PopBackStack : AddEditVocabularyEvent

    data class ShowToast(@StringRes val stringRes: Int) : AddEditVocabularyEvent
}