package me.asddsajpgnoob.quizandvocabularyrx.data.repository

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import me.asddsajpgnoob.quizandvocabularyrx.data.model.room.VocabularyCardEntity

interface VocabularyCardRepository {

    fun searchVocabularyCardsByOriginOrTranslationOrderByCreatedAtDesc(query: String, offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    fun findRandomVocabularyCard(exceptionIds: List<Long>): Maybe<VocabularyCardEntity>

    fun findLessGuessedVocabularyCards(offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    fun findNewestVocabularyCards(offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    fun findOldestVocabularyCards(offset: Int, limit: Int): Single<List<VocabularyCardEntity>>

    fun insert(entity: VocabularyCardEntity): Single<Long>

    fun update(entity: VocabularyCardEntity): Completable

    fun delete(entity: VocabularyCardEntity): Completable
}