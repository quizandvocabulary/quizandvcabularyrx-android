package me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.*
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.fragment.navArgs
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import me.asddsajpgnoob.quizandvocabularyrx.R
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.InterfaceSettingsRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.data.repository.impl.VocabularyCardRepositoryImpl
import me.asddsajpgnoob.quizandvocabularyrx.databinding.FragmentAddEditVocabularyCardBinding
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.adapter.RemovableAudiosWithFooterAdapter
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model.AddEditVocabularyEvent
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model.AudioListItem
import me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model.ExampleListItem
import me.asddsajpgnoob.quizandvocabularyrx.ui.base.BaseFragment
import me.asddsajpgnoob.quizandvocabularyrx.ui.dialog.recordAudioDialog.RecordAudioDialogFragment
import me.asddsajpgnoob.quizandvocabularyrx.ui.main.MainViewModel
import me.asddsajpgnoob.quizandvocabularyrx.util.Constants
import me.asddsajpgnoob.quizandvocabularyrxrx.ui.addEditVocabularyCard.adapter.EditableExamplesWithFooterAdapter
import me.asddsajpgnoob.quizandvocabularyrxrx.util.IntentUtils
import java.io.File

@ExperimentalCoroutinesApi
class AddEditVocabularyCardFragment : BaseFragment<FragmentAddEditVocabularyCardBinding>(),
    EditableExamplesWithFooterAdapter.ExamplesAdapterListener,
    RemovableAudiosWithFooterAdapter.AudiosAdapterListener {

    companion object {
        const val CURRENT_DESTINATION_ID = R.id.addEditVocabularyCardFragment
    }

    private val nevArgs: AddEditVocabularyCardFragmentArgs by navArgs()

    private val examplesAdapter = EditableExamplesWithFooterAdapter(this)

    private val audiosAdapter = RemovableAudiosWithFooterAdapter(this)

    private val viewModel: AddEditVocabularyCardViewModel by viewModels {
        AddEditVocabularyCardViewModel.Factory(
            this,
            nevArgs.toBundle(),
            InterfaceSettingsRepositoryImpl,
            VocabularyCardRepositoryImpl,
            requireContext().getExternalFilesDir(null)!!,
        )
    }

    override val mainViewModel: MainViewModel by activityViewModels {
        MainViewModel.Factory(InterfaceSettingsRepositoryImpl)
    }

    override fun getViewBinding() = FragmentAddEditVocabularyCardBinding.inflate(layoutInflater)

    override fun getOrientation() = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        setFragmentResultListeners()

        setupView()
        setupRecyclerViews()
        setOnTextChangeListeners()
        setOnClickListeners()
        setOnCheckedChangedListener()

        collectShowInQuiz()
        collectExamples()
        collectPlaybackInfo()
        collectAudioFiles()
        collectSubmitButtonEnabled()

        collectEvents()

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_edit_vocabulary_card, menu)
        if (viewModel.initialVocabularyCard == null) {
            val itemDeleteCard = menu.findItem(R.id.item_delete_card)
            itemDeleteCard.isVisible = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_delete_card -> {
                viewModel.onMenuItemDeleteCardSelected()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun setFragmentResultListeners() {
        parentFragmentManager.setFragmentResultListener(
            RecordAudioDialogFragment.REQUEST_KEY_RECORD_AUDIO,
            this
        ) { _, bundle ->
            viewModel.onAudioRecorded(bundle.getSerializable(RecordAudioDialogFragment.KEY_AUDIO_FILE) as File)
        }
    }

    private fun setupView() {
        @StringRes val submitStringRes = if (viewModel.initialVocabularyCard == null) {
            R.string.Add
        } else {
            R.string.Save
        }
        binding.apply {
            buttonSubmit.setText(submitStringRes)

            editTextOrigin.setText(viewModel.getCurrentOrigin().value)
            editTextTranslation.setText(viewModel.getCurrentTranslation().value)

            if (viewModel.initialVocabularyCard != null) {
                textGuessedCount.text = getString(
                    R.string.Guessed_count_colon_number,
                    viewModel.initialVocabularyCard!!.guessedCount
                )
                textGuessedCount.isVisible = true

                textNotGuessedCount.text = getString(
                    R.string.Not_guessed_count_colon_number,
                    viewModel.initialVocabularyCard!!.notGuessedCount
                )
                textNotGuessedCount.isVisible = true
            } else {
                textGuessedCount.isVisible = false
                textNotGuessedCount.isVisible = false
            }
        }
    }

    private fun setupRecyclerViews() {
        binding.apply {
            recyclerViewExamples.setHasFixedSize(false)
            recyclerViewExamples.adapter = examplesAdapter

            recyclerViewAudios.setHasFixedSize(false)
            recyclerViewAudios.adapter = audiosAdapter
        }
    }

    private fun setOnTextChangeListeners() {
        binding.apply {
            editTextOrigin.doAfterTextChanged {
                viewModel.onEditTextOriginTextChanges(it?.toString())
            }
            editTextTranslation.doAfterTextChanged {
                viewModel.onEditTextTranslationChanges(it?.toString())
            }
        }
    }

    private fun setOnClickListeners() {
        binding.apply {
            buttonSubmit.setOnClickListener {
                viewModel.onButtonSubmitClicked()
            }
            imageButtonSearchOrigin.setOnClickListener {
                viewModel.onImageButtonSearchOriginClicked()
            }
        }
    }

    private fun setOnCheckedChangedListener() {
        binding.apply {
            checkBoxShowInQuiz.setOnCheckedChangeListener { _, isChecked ->
                viewModel.onCheckboxShowInQuizCheckedChange(isChecked)
            }
        }
    }

    private fun collectShowInQuiz() {
        viewDisposable.add(
            viewModel.showInQuiz
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.checkBoxShowInQuiz.isChecked = it
                }
        )
    }

    private fun collectExamples() {
        viewDisposable.add(
            viewModel.examplesListItems
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    examplesAdapter.submitListData(it)
                }
        )
    }

    private fun collectPlaybackInfo() {
        viewDisposable.add(
            viewModel.playbackInfo
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    audiosAdapter.updatePlaybackInfo(it.value)
                }
        )
    }

    private fun collectAudioFiles() {
        viewDisposable.add(
            viewModel.audioListItems
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    audiosAdapter.submitListData(viewModel.getCurrentPlaybackInfo().value, it)
                }
        )
    }

    private fun collectSubmitButtonEnabled() {
        viewDisposable.add(
            viewModel.submitButtonEnabled
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.buttonSubmit.isEnabled = it
                }
        )
    }

    private fun collectEvents() {
        viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onStart(owner: LifecycleOwner) {
                viewModel.events.subscribe {
                    viewDisposable.add(
                        Single.just(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { event ->
                                when (event) {
                                    AddEditVocabularyEvent.NavigateToRecordAudioDialog -> {
                                        val action =
                                            AddEditVocabularyCardFragmentDirections.actionAddEditVocabularyCardFragmentToRecordAudioDialogFragment()
                                        navigateIfInDestination(CURRENT_DESTINATION_ID, action)
                                    }
                                    is AddEditVocabularyEvent.NavigateToGoogleTranslate -> {
                                        val intent = IntentUtils.getActionViewIntent(
                                            String.format(
                                                Constants.URL_GOOGLE_TRANSLATE_UNFORMATTED,
                                                event.origin
                                            )
                                        )
                                        if (!event.openOnTopOfApp) {
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        }
                                        startActivity(intent)
                                    }
                                    AddEditVocabularyEvent.PopBackStack -> {
                                        popBackStack()
                                    }
                                    is AddEditVocabularyEvent.ShowToast -> {
                                        showToast(event.stringRes)
                                    }
                                }
                            }
                    )
                }
            }

            override fun onStop(owner: LifecycleOwner) {
                viewModel.events.unsubscribe()
            }
        })
    }

    override fun onDeleteExampleClick(item: ExampleListItem.Example) {
        viewModel.onDeleteExampleClicked(item)
    }

    override fun onExamplesEdited(position: Int, newValue: String) {
        viewModel.onExamplesEdited(position, newValue)
    }

    override fun onAddExampleClicked() {
        viewModel.onAddExampleClicked()
    }

    override fun onPlayPauseClicked(audioItem: AudioListItem.Audio) {
        viewModel.onPlayPauseClicked(audioItem)
    }

    override fun onDeleteAudioClicked(item: AudioListItem.Audio) {
        viewModel.onDeleteAudioClicked(item)
    }

    override fun onAddAudioClicked() {
        viewModel.onAddAudioClicked()
    }

}