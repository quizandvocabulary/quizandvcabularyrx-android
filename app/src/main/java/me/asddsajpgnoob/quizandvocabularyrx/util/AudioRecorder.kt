package me.asddsajpgnoob.quizandvocabularyrx.util

import android.content.Context
import android.media.MediaRecorder
import android.os.Build
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.*
import me.asddsajpgnoob.quizandvocabularyrx.App
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

class AudioRecorder private constructor(
    private val context: Context,
    private val audioSource: Int,
    private val outputFormat: Int,
    private val audioEncoder: Int,
    private val fileNameGenerator: () -> String,
    private var saveToDirectory: File
) {

    companion object {
        const val RECORDING_TIME_NO_LIMIT = 0L
    }

    private var recordingTime = 0L

    var isRecording: Boolean = false
        private set

    var outputPath: String? = null
        private set

    private var timerJob: Job? = null

    private var onRecordingTimeChangesListener: ((milliseconds: Long) -> Unit)? = null

    private var mediaRecorder: MediaRecorder? = null

    fun removeLastOutput() {
        outputPath?.let {
            File(it).delete()
        }
        outputPath = null
    }

    fun setOnRecordingTimeChangesListener(listener: (milliseconds: Long) -> Unit) {
        this.onRecordingTimeChangesListener = listener
    }

    fun startRecording(
        recordingTimeLimit: Long = RECORDING_TIME_NO_LIMIT,
        onLimitReachedListener: (() -> Unit)? = null
    ) {
        if (isRecording) {
            throw IllegalStateException("Already recording")
        }
        recordingTime = 0L
        init()
        mediaRecorder!!.apply {
            prepare()
            start()
        }
        isRecording = true
        onRecordingTimeChangesListener?.invoke(recordingTime)
        val startTime = System.currentTimeMillis()
        Observable.interval(100L,100L, TimeUnit.MILLISECONDS)
            .takeWhile {
                isRecording
            }.subscribe {
                recordingTime = System.currentTimeMillis() - startTime
                onRecordingTimeChangesListener?.invoke(recordingTime)
                if (recordingTimeLimit in (RECORDING_TIME_NO_LIMIT + 1)..recordingTime) {
                    stopRecording()
                    onLimitReachedListener?.invoke()
                }
            }
    }

    @Throws(RuntimeException::class)
    fun stopRecording() {
        if (!isRecording) {
            throw IllegalStateException("Not recording")
        }
        timerJob?.cancel()
        timerJob = null
        recordingTime = 0L
        mediaRecorder!!.apply {
            stop()
            release()
        }
        isRecording = false
    }

    private fun init() {
        mediaRecorder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            MediaRecorder(context)
        } else {
            @Suppress("deprecation")
            MediaRecorder()
        }

        mediaRecorder!!.apply {
            setAudioSource(audioSource)
            setOutputFormat(outputFormat)
            setAudioEncoder(audioEncoder)
        }

        val fileName = fileNameGenerator.invoke()

        outputPath = "${saveToDirectory.absolutePath}/$fileName.aac"

        mediaRecorder!!.setOutputFile(outputPath!!)
    }

    class Builder(private val context: Context) {
        private var audioSource: Int = MediaRecorder.AudioSource.MIC
        private var outputFormat: Int = MediaRecorder.OutputFormat.AAC_ADTS
        private var audioEncoder: Int = MediaRecorder.AudioEncoder.AAC
        private var saveToDirectory: File = context.cacheDir
        private var fileNameGenerator: () -> String = {
            "${UUID.randomUUID()}_${DateUtils.dateToString(Date(), pattern = DateUtils.FILE_NAME_FORMAT)}"
        }

        fun setAudioSource(audioSource: Int): Builder {
            this.audioSource = audioSource
            return this
        }

        fun setOutputFormat(outputFormat: Int): Builder {
            this.outputFormat = outputFormat
            return this
        }

        fun setAudioEncoder(audioEncoder: Int): Builder {
            this.audioEncoder = audioEncoder
            return this
        }

        fun saveRecordingsToDirectory(saveToDirectory: File): Builder {
            this.saveToDirectory = saveToDirectory
            return this
        }

        fun setFileNameGenerator(fileNameGenerator: () -> String): Builder {
            this.fileNameGenerator = fileNameGenerator
            return this
        }

        fun build() = AudioRecorder(
            context,
            audioSource,
            outputFormat,
            audioEncoder,
            fileNameGenerator,
            saveToDirectory
        )
    }

}