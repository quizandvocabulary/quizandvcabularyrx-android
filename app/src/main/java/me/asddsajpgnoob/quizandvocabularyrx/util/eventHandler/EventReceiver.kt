package me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler

import me.asddsajpgnoob.quizandvocabularyrx.util.KOptional

abstract class EventReceiver<T> {

    protected val lock = Any()

    protected val capacity = 16

    protected val unconsumedValues = Array<KOptional<T>>(capacity) {
        KOptional.empty()
    }

    @Volatile
    protected var onNext: ((T) -> Unit)? = null

    fun subscribe(onNext: (T) -> Unit) {
        this.onNext = onNext
        synchronized(lock) {
            for (i in 0 until capacity) {
                val v = unconsumedValues[i]
                if (v.value == null) {
                    break
                }
                val listener = this.onNext
                if (listener == null) {
                    var n: KOptional<T> = v
                    var j = 0
                    while (n.value != null) {
                        unconsumedValues[j] = n
                        unconsumedValues[i] = KOptional.empty()
                        j++
                        val index = i + j
                        n = if (index == capacity) {
                            KOptional.empty()
                        } else {
                            unconsumedValues[i + j]
                        }
                    }
                    break
                }
                listener.invoke(v.value)
                unconsumedValues[i] = KOptional.empty()
            }
        }
    }

    fun unsubscribe() {
        this.onNext = null
    }

}