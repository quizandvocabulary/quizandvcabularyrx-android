package me.asddsajpgnoob.quizandvocabularyrx.data.model.room

import androidx.room.*
import me.asddsajpgnoob.quizandvocabularyrx.data.local.typeConverter.DateTypeConverter
import me.asddsajpgnoob.quizandvocabularyrx.data.local.typeConverter.StringListTypeConverter
import java.util.*

@TypeConverters(
    value = [
        StringListTypeConverter::class,
        DateTypeConverter::class
    ]
)
@Entity(
    tableName = "vocabulary_cards",
    indices = [
        Index(value = ["origin"], unique = false),
        Index(value = ["translation"], unique = false)
    ]
)
data class VocabularyCardEntity(
    @ColumnInfo(name = "origin")
    val origin: String,

    @ColumnInfo(name = "translation")
    val translation: String,

    @ColumnInfo(name = "examples")
    val examples: List<String>,

    @ColumnInfo(name = "audio_files")
    val audioFiles: List<String>,

    @ColumnInfo(name = "show_in_quiz")
    val showInQuiz: Boolean,

    @ColumnInfo(name = "guessed_info")
    val guessedCount: Int,

    @ColumnInfo(name = "not_guessed_count")
    val notGuessedCount: Int,

    @ColumnInfo(name = "created_at")
    val createdAt: Date,

    @ColumnInfo(name = "updatedAt")
    val updatedAt: Date,

    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L
)
