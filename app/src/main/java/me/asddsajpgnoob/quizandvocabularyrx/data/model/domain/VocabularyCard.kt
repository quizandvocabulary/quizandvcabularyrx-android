package me.asddsajpgnoob.quizandvocabularyrx.data.model.domain

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class VocabularyCard(
    val origin: String,

    val translation: String,

    val examples: List<String>,

    val audioFiles: List<String>,

    val showInQuiz: Boolean,

    val guessedCount: Int,

    val notGuessedCount: Int,

    val createdAt: Date,

    val updatedAt: Date,

    val id: Long
): Parcelable