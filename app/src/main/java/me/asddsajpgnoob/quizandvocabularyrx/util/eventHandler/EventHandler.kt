package me.asddsajpgnoob.quizandvocabularyrx.util.eventHandler

import me.asddsajpgnoob.quizandvocabularyrx.util.kOptional

class EventHandler<T> : EventReceiver<T>() {

    fun onNext(value: T) {
        onNext.let { listener ->
            if (listener != null) {
                listener.invoke(value)
            } else {
                synchronized(lock) {
                    for (i in 0 until capacity) {
                        if (unconsumedValues[i].value == null) {
                            unconsumedValues[i] = value.kOptional()
                            break
                        }
                    }
                }
            }
        }
    }

}