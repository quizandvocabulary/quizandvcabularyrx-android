package me.asddsajpgnoob.quizandvocabularyrx.ui.addEditVocabularyCard.model

sealed interface ExampleListItem {

    data class Example(val example: String) : ExampleListItem

    object Footer : ExampleListItem
}