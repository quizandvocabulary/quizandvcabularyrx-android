package me.asddsajpgnoob.quizandvocabularyrx.data.repository

import io.reactivex.rxjava3.core.Flowable
import me.asddsajpgnoob.quizandvocabularyrx.data.model.dataStore.QuizModePreferences

interface QuizSettingsRepository {

    val quizMode: Flowable<QuizModePreferences>

    fun setQuizMode(quizMode: QuizModePreferences)
}